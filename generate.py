#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

"""
import numpy as np
import pylab
import os.path
from common_AtomPos import pproj, R, Eps_u, Eps_bi
from evaluate_strains import is_error


def generateAtomsVect(i,j,k,l,m,n,q,r,Nmaille):
    ''' 
    generates vectors for the commensurate cell
    
    Parameters
    ----------
    i, j, k, l, m, n, q, r: commensurate integers
    Nmaille : Commensurability order

    Returns
    -------
    ab1, ab2 : 1D arrays bottom layer periodicity
    at1, at2 : 1D arrays strained layer periodicity
    am1, am2 : 1D arrays moiré periodicity
    nAm1, nAm2 : float moiré norms
    '''
    i = float(i)
    j = float(j)
    k = float(k)
    l = float(l)
    m = float(m)
    n = float(n)
    q = float(q)
    r = float(r)

    #Define basisvectors of substrate in units of a_TM
    ab1=np.array([1.,0])
    ab2=np.array([-1./2,np.sqrt(3)/2])

    #Find moiré vector
    am1=m*ab1+n*ab2
    am2=q*ab1+r*ab2
    #Find graphene vector
    D=i*l-j*k
    if(D==0): print('D=0!')
    at1=(l*am1-j*am2)/D
    at2=(i*am2-k*am1)/D

    #print(ag1,ag2,am1,am2,as1,as2)

    nAm1=(Nmaille*pylab.linalg.norm(am1))
    nAm2=(Nmaille*pylab.linalg.norm(am2))

    return (ab1,ab2,at1,at2,am1,am2,nAm1,nAm2)

def generateAtomsPos (i,j,k,l,m,n,q,r,Nmaille,header,c_gr='red',c_s='blue') :
    ''' 
    generates atomic positions in the commensurate cell & writes them to file 
    starting from commensurate integers
    
    Parameters
    ----------
    i, j, k, l, m, n, q, r: commensurate integers
    Nmaille : Commensurability order

    Returns
    -------
    log : information about atomic positions. Also printed.
    Xpt_gr, Ypt_gr : arrays for atomic positions of strained (graphene) layer
    Xpt_s, Ypt_s : arrays for atomic positions of unstrained (support) layer
    f : Gen_files/AtomPos_header.txt contains useful information, Atom pos.

    '''
    as1,as2,ag1,ag2,am1,am2,nAm1,nAm2 = generateAtomsVect(i,j,k,l,m,n,q,r,Nmaille)

    log=""

    Xpt_gr=[]
    Ypt_gr=[]
    Xpt_s=[]
    Ypt_s=[]
    
    try:
        if header=='Test':
            f = open(os.path.join('Gen_files', 'Test.txt'), "w")
        else:
            f = open(os.path.join('Gen_files', header),
                     "w")
    except IOError:
        f = open("Pos.txt", "w")
        print('Error in opening the usual output file. Positions were saved in ./Pos.txt.')
    f.write("# "+header+"\n")
    f.write("#2.46(Ang) = a maille. \t Biplan : i="+str(i)+" j="+str(j)+" k="+str(k)+" l="+str(l)+" m="+str(m)+" n="+str(n)+" q="+str(q)+" r="+str(r)+"\n")
    f.write("#\t"+str(am1[0])+"\t"+str(am1[1])+"\t 0\t vecteur de maille a1 (unite de a)\n")
    f.write("#\t"+str(am2[0])+"\t"+str(am2[1])+"\t 0\t vecteur de maille a2 (unite de a)\n")
    #f.write("#\t"+str(4*(N**2))+"\t Nb atomes dans la maille\n")
    #f.write("#\t 2 \t Nb atomes inequivalents\n")
    f.write("# X\t Y \t Z \t SL\n")
    grA=0
    grB=0
    sA=0
    sB=0
    
    for x in range(-int(Nmaille*(abs(i)+abs(k))),int(Nmaille*(abs(i)+abs(k)))):
        for y in range(-int(Nmaille*(abs(j)+abs(l))),int(Nmaille*(abs(j)+abs(l)))):
            vecA=x*ag1+y*ag2
            vecB=(x+2./3)*ag1+(y+1./3)*ag2
            pvecA=pproj(vecA,am1,am2)
            pvecB=pproj(vecB,am1,am2)
            #if(True):
            if(pvecA[0]>= -0.0000001 and pvecA[0]< 0.9999999*nAm1 and pvecA[1]>=-0.0000001 and pvecA[1]< 0.9999999*nAm2):         
                Xpt_gr.append(vecA[0])
                Ypt_gr.append(vecA[1])
                f.write(str(vecA[0])+"\t"+str(vecA[1])+"\t"+str(0.0)+"\t"+"A"+"\n")
                grA=grA+1
            if(pvecB[0]>= -0.0000001 and pvecB[0]< 0.9999999*nAm1 and pvecB[1]>= -0.0000001 and pvecB[1]< 0.9999999*nAm2):
                Xpt_gr.append(vecB[0])
                Ypt_gr.append(vecB[1])
                f.write(str(vecB[0])+"\t"+str(vecB[1])+"\t"+str(0.0)+"\t"+"B"+"\n")
                grB=grB+1
    for x in range(-int(Nmaille*(abs(m)+abs(q))),int(Nmaille*(abs(m)+abs(q)))):
        for y in range(-int(Nmaille*(abs(n)+abs(r))),int(Nmaille*(abs(n)+abs(r)))):
            vecA=x*as1+y*as2
            vecB=(x+2./3)*as1+(y+1./3)*as2
            pvecA=pproj(vecA,am1,am2)
            pvecB=pproj(vecB,am1,am2)
            #if(True):
            if(pvecA[0]>= -0.0000001 and pvecA[0]< 0.9999999*nAm1 and pvecA[1]>= -0.0000001 and pvecA[1]< 0.9999999*nAm2):         
                Xpt_s.append(vecA[0])
                Ypt_s.append(vecA[1])
                f.write(str(vecA[0])+"\t"+str(vecA[1])+"\t"+str(1.36345)+"\t"+"A"+"\n")
                sA=sA+1
            if(pvecB[0]>= -0.0000001 and pvecB[0]< 0.9999999*nAm1 and pvecB[1]>= -0.0000001 and pvecB[1]< 0.9999999*nAm2):
                Xpt_s.append(vecB[0])
                Ypt_s.append(vecB[1])
                f.write(str(vecB[0])+"\t"+str(vecB[1])+"\t"+str(1.36345)+"\t"+"B"+"\n")
                sB=sB+1
    #pylab.figure()
    

    #Plot moiré lattice vectors
    pylab.plot([0,am1[0]],[0,am1[1]],color="black")
    pylab.plot([0,am2[0]],[0,am2[1]],color="black")
    pylab.plot([am1[0],am1[0]+am2[0]],[am1[1],am1[1]+am2[1]],color="green")
    pylab.plot([am2[0],am2[0]+am1[0]],[am2[1],am2[1]+am1[1]],color="green")
    #pylab.plot([am1[0],am2[0]],[am1[1],am2[1]],color="black")
    
    #Plot substrate lattice vectors
    pylab.plot([0,as1[0]],[0,as1[1]],color=c_s)
    pylab.plot([0,as2[0]],[0,as2[1]],color=c_s)
    
    #Plot graphene lattice vectors
    pylab.plot([0,ag1[0]],[0,ag1[1]],color=c_gr)    
    pylab.plot([0,ag2[0]],[0,ag2[1]],color=c_gr)


    #Plot atomic positions
    pylab.plot(Xpt_s,Ypt_s,ls='None',marker='o',color=c_s,markersize=2.,markeredgewidth=0.)#,rasterized=True)
    pylab.plot(Xpt_gr,Ypt_gr,ls='None',marker='o',color=c_gr,markersize=2.,markeredgewidth=0.)#,rasterized=True)
    
    

    pylab.axes().set_aspect('equal')
    f.close()
    
    log=log+"\n*** ATOMIC POSITIONS GENERATION ***\n\n"
    log=log+"Number of atoms in:\n"
    log=log+"Sublattice A of gr:"+str(grA)+"\n"
    log=log+"Sublattice B of gr:"+str(grB)+"\n"
    log=log+"Gr plan:"+str(grA+grB)+"\n"
    log=log+"Sublattice A of support:"+str(sA)+"\n"
    log=log+"Sublattice B of support:"+str(sB)+"\n"
    log=log+"Support plan:"+str(sA+sB)+"\n"
    log=log+"Total:"+str(grA+grB+sA+sB)+"\n"
    
    #Return atomic postions and log
    print(log)
    return np.array(Xpt_gr), np.array(Ypt_gr), np.array(Xpt_s), np.array(Ypt_s), log#.encode('utf8')



def generate_comm(theta1, theta_interlayer, p_iso, p_an, precision=0.0001):
    ''' function that yields (ijklmnqr) parameters
    that are fit for generating a commensurate cell,
    starting from physical strain parameters.
    
    Parameters
    ----------
    theta1 : strain direction in degrees
    theta_interlayer : twist angle in degrees
    p_iso : biaxial strain param (float>1)
    p_an : uniaxial strain param (float>1)
    precision : how precise should be park madden coeficients
    (more precise PM yields bigger cells)

    Returns
    -------
    ii, jj, kk, ll, mm, nn, qq, rr: commensurate integers
    ab1, ab2 : 1D arrays bottom layer periodicity
    at1_new, at2_new : 1D arrays strained layer periodicity
    am1, am2 : 1D arrays moiré periodicity
    nAm1, nAm2 : float moiré norms
    p_iso, p_an, theta_1, theta_2, theta_int_new :
        physical strain parameters that fit commensurability
        can be slightly dir=fferent from input
    N : Commensurability order
    '''

    print('Atom_pos for theta_interlayer=%.1f deg, theta_1=%.1f deg, p_iso=%.4f, p_an=%.4f' % (theta_interlayer, theta1, p_iso, p_an))

    # Passage matrix
    P = np.array([[1., -0.5],[0, 1.0*np.sqrt(3)/2.]], dtype='float32')

    # Define basisvectors of bottom graphene in orthogonal basis
    ab1 = np.array([1., 0])
    ab2 = np.array([-1./2, np.sqrt(3)/2])

    # Define initial basisvectors of top graphene in orthogonal basis
    at1 = ab1
    at2 = ab2

    # Find New at1 at2 without theta2 rotation, in the orthogonal basis
    at1_intermediate = np.dot( Eps_u(p_an),
                              np.dot( R(theta1) , np.dot( Eps_bi(p_iso) , at1)))
    at2_intermediate = np.dot( Eps_u(p_an),
                              np.dot( R(theta1) , np.dot( Eps_bi(p_iso) , at2)))

    # Find the intermediate angle between layers
    # The 3 directions should be taken into account
    thetaintermediaire1 = np.degrees( np.arctan2( at1_intermediate[1], at1_intermediate[0] ) ) - np.degrees( np.arctan2( ab1[1], ab1[0] ) )
    thetaintermediaire2 = np.degrees( np.arctan2( at2_intermediate[1], at2_intermediate[0] ) ) - np.degrees( np.arctan2( ab2[1], ab2[0] ) )
    if at2_intermediate[1] < 0 : thetaintermediaire2 = thetaintermediaire2 +360 # if the vectore are on different sides of 90deg, the previous calculation does not hold
    thetaintermediaire3 = np.degrees( np.arctan2( (at1_intermediate[1]+at2_intermediate[1]), (at2_intermediate[0]+at1_intermediate[0]) ) ) - np.degrees( np.arctan2( (ab1[1]+ab2[1]), (ab1[0]+ab2[0]) ) )
    if (at1_intermediate[1]+at2_intermediate[1]) < 0 : thetaintermediaire3 = 360 + thetaintermediaire3  # if the vectore are on different sides of 90deg, the previous calculation does not hold
    if np.sign(at2_intermediate[0]) == np.sign(at1_intermediate[0]) and np.sign(at2_intermediate[1]) == np.sign(at1_intermediate[1]) : print("be careful! p_an is too big")
    thetaintermediaire = (thetaintermediaire1 + thetaintermediaire2 + thetaintermediaire3)/3.

    # Find New at1 at2, in the orthogonal basis
    theta2 = theta_interlayer - thetaintermediaire
    at1_new = np.dot(R(theta2), at1_intermediate)
    at2_new = np.dot(R(theta2), at2_intermediate)

    # Find New at1 at2, in the hexagonal basis
    at1_new_hex = np.dot(np.linalg.inv(P), at1_new)
    at2_new_hex = np.dot(np.linalg.inv(P), at2_new)
    # print(at1_new_hex,at2_new_hex)
   
    #Find the final angle between layers, should be theta_interlayer if it has worked well
    thetanew1 = np.degrees(np.arctan(at1_new[1]/at1_new[0])) - np.degrees(np.arctan(ab1[1]/ab1[0]))
    thetanew2 = np.degrees(np.arctan(at2_new[1]/at2_new[0]))- np.degrees(np.arctan(ab2[1]/ab2[0]))
    thetanew3 = np.degrees(np.arctan((at1_new[1]+at2_new[1])/(at2_new[0]+at1_new[0]))) - np.degrees(np.arctan((ab1[1]+ab2[1])/(ab1[0]+ab2[0])) )  # should not hold for big final angles

    print('goal theta interlayer :'+str(theta_interlayer))
    print('theta1 applied during transformation = '+str(theta1))
    print('thetaintermediare before thetha2 transformation = '+str(thetaintermediaire))
    print('thetaintermediare1 before thetha2 transformation = '+str(thetaintermediaire1))
    print('thetaintermediare2 before thetha2 transformation = '+str(thetaintermediaire2))
    print('thetaintermediare3 before thetha2 transformation = '+str(thetaintermediaire3))
    print('theta2 applied during transformation = '+str(theta2))
    print( '\n new theta interlayer in Bortho : \n *'+str( thetanew1 ) +' for first vector\n *'+str(thetanew2)+' for second vector\n *'+str(thetanew3)+' for third direction \n')

    #New park madden Matrix
    a = at1_new_hex[0]
    b = at1_new_hex[1]
    c = at2_new_hex[0]
    d = at2_new_hex[1] 
    print('**\n'+str(a) +' '+str(b) +' '+str(c) +' '+str(d) )

    det = ((1 - a) * (1 - d) - (b * c) )
    i = (1 - d) / det
    j = -(-b) / det
    k = -(-c) / det
    l = (1 - a) / det
    m = i * a + j * c
    n = i * b + j * d
    q = k * a + l * c
    r = b * k + d * l
    print('**\n'+str(i) +' '+str(j) +' '+str(k) +' '+str(l) + str(m) +' '+str(n) +' '+str(q) +' '+str(r) )

    #Make it a commensurate cell
    ii = round(i)
    jj = round(j)
    kk = round(k)
    ll = round(l)
    mm = round(m)
    nn = round(n)
    qq = round(q)
    rr = round(r)

    N = 1
    iter = 2

    while ( abs(((1/(ii*ll-kk*jj))*(ll*mm-jj*qq)) -  a) > precision ) :#and (N<5): #or ( abs(((1/(ii*ll-kk*jj))*(ii*qq-kk*mm)) -  b) > 0.0001 ):#on veut que les quatres decimales soient bonnes
        N=iter
        ii = round(i*N)
        jj = round(j*N)
        kk = round(k*N)
        ll = round(l*N)
        mm = round(m*N)
        nn = round(n*N)
        qq = round(q*N)
        rr = round(r*N)
        iter += 1
    print('\n**commensurate indexes'+str(ii) +' '+str(jj) +' '+str(kk) +' '+str(ll) + str(mm) +' '+str(nn) +' '+str(qq) +' '+str(rr)+'\n')
    print('we find a cell of order' + str(N))

    # Find moiré vector
    am1 = ii*at1_new+jj*at2_new
    am2 = kk*at1_new+ll*at2_new
    nAm1 = (N*pylab.linalg.norm(am1))
    nAm2 = (N*pylab.linalg.norm(am2))

    # Find new a,b,c,d
    a = (1/(ii*ll-kk*jj))*(ll*mm-jj*qq)
    b = (1/(ii*ll-kk*jj))*(ll*nn-jj*rr)
    c = (1/(ii*ll-kk*jj))*(ii*qq-kk*mm)
    d = (1/(ii*ll-kk*jj))*(ii*rr-kk*nn)
    print('\n** new a,b,c,d'+str(a)+str(b)+str(c)+str(d))

    # attention à la définition (celle là est physiquement bonne)
    theta_sum = pylab.arctan2(-a + 2*b - 2*c+d, (a+d)*pylab.sqrt(3))
    theta_dif = pylab.arctan2(a + b + 2*c-d, (a-b-d)*pylab.sqrt(3))
    # if theta2 - theta1 < ou > 180, arctg2 ne calcule pas la "vraie" valeur !
    if theta1 >= 90: theta_dif = - 2*np.pi + theta_dif

    theta_2 = 180./pylab.pi * 1./2 * (theta_sum + theta_dif)
    theta_1 = 180./pylab.pi * 1./2 * (theta_sum - theta_dif)
    print('\n****new theta1, theta2')
    print(theta_1, theta_2)

    P_1 = a**2 + b**2 - a*b
    DeltaP1 = 1e-5*np.sqrt(5*(a**2 + b**2) - 8*a*b)
    P_2 = c**2 + d**2 - c*d
    DeltaP2 = 1e-5*np.sqrt(5*(c**2 + d**2) - 8*c*d)
    C = 2*(a*c + b*d) - (a*d + b*c)
    DeltaC = 1e-5*np.sqrt(5*(a**2 + b**2+c**2 + d**2) - 8*(a*b+c*d))
    p_iso = pylab.sqrt((2*P_1+2*P_2+C)/3) * pylab.sqrt(1 - pylab.sqrt(1 - 3*(4*P_1*P_2-C**2)/(2*P_1+2*P_2+C)**2))
    if p_iso != p_iso:  # detects situation when p_iso is not a number (argument of sqrt negative)
        p_iso = pylab.sqrt((2*P_1+2*P_2+C)/3) * pylab.sqrt(abs(1 - pylab.sqrt(abs(1 - 3*(4*P_1*P_2-C**2)/(2*P_1+2*P_2+C)**2))))
    p_an  = (2*P_1+2*P_2+C)/pylab.sqrt(3) * 1/pylab.sqrt(4*P_1*P_2-C**2) * (1 + pylab.sqrt(1 - 3*(4*P_1*P_2-C**2)/(2*P_1+2*P_2+C)**2))
    if p_an != p_an:  # detects situation when p_iso is not a number (argument of sqrt negative)
        p_an  = (2*P_1+2*P_2+C)/pylab.sqrt(3) * 1/pylab.sqrt(4*P_1*P_2-C**2) * (abs(1 + pylab.sqrt(abs(1 - 3*(4*P_1*P_2-C**2)/(2*P_1+2*P_2+C)**2))))
    W = 2*P_1+2*P_2+C
    Z = 4*P_1*P_2-C**2

    # is error ok ?
    is_error(p_an, p_iso, det, a, b, c, d, i, N,
             DeltaP1, DeltaP2, DeltaC, P_1, P_2, C, Z, W)

    e_bi = p_iso - 1
    e_uni = (p_iso * p_an - p_iso)
    print('eps_uni='+str(e_uni)+', eps_bi ='+str(e_bi))
    if (p_iso > 1 and p_an < 1) or (p_iso < 1 and p_an > 1):
        print("\nNot good!"+"\n")
        p_iso_2 = p_an * p_iso
        p_an_2 = 1 / p_an
        e_bi_2 = p_iso_2 - 1
        e_uni_2 = (p_iso_2 * p_an_2 - p_iso_2)
        if abs(e_bi_2)<abs(e_bi) and abs(e_uni_2) < abs(e_uni):# (abs(e_bi_2) + abs(e_uni_2)) < (abs(e_uni) + abs(e_bi)) : 
            e_bi = e_bi_2; e_uni = e_uni_2
            print('eps_uni='+str(e_uni)+', eps_bi ='+str(e_bi))
            p_iso = p_iso_2
            p_an  = p_an_2
            theta_1 += 90.
            theta_2 -= 90.
            print('p_iso='+str(p_iso)+', p_an ='+str(p_an))
            print('theta1='+str(theta_1)+', theta2 ='+str(theta_2))
        print('eps_uni='+str(e_uni_2)+', eps_bi ='+str(e_bi_2))

    # Find final angle between layers
    alpha_1 = (180./pylab.pi) * pylab.arctan2(b*pylab.sqrt(3), (2*a-b))
    alpha_2 = (180./pylab.pi) * pylab.arctan2(c*pylab.sqrt(3), (c-2*d))
    if alpha_1 < 0.:
        while alpha_1 < 0.: alpha_1 += 60.
    elif alpha_1 > 60.:
        while alpha_1 > 60.: alpha_1 -= 60.
    if alpha_2 < 0.:
        while alpha_2 < 0.: alpha_2 += 60.
    elif alpha_2 > 60.:
        while alpha_2 > 60.: alpha_2 -= 60.
    vec_gr_1 = pylab.array([pylab.sqrt(P_1)*pylab.cos((pylab.pi/180.)*alpha_1), pylab.sqrt(P_1)*pylab.sin((pylab.pi/180.)*alpha_1)])  # a_gr_1 in cartesian coordinates
    vec_gr_2 = pylab.array([pylab.sqrt(P_2)*pylab.cos((pylab.pi/180.)*alpha_2+2*pylab.pi/3), pylab.sqrt(P_2)*pylab.sin((pylab.pi/180.)*alpha_2+2*pylab.pi/3)])  # a_gr_2 in cartesian coordinates
    vec_gr_3 = vec_gr_1 + vec_gr_2 # gives the third direction of graphene
    alpha_3 = (180./pylab.pi) * pylab.arctan2(vec_gr_3[1], vec_gr_3[0]) - 60.
    theta_int_new = (alpha_1 + alpha_2 + alpha_3) / 3.

    return (ii, jj, kk, ll, mm, nn, qq, rr,
            ab1, ab2, at1_new, at2_new, am1, am2, nAm1, nAm2, 
            p_iso, p_an, theta_1, theta_2, N, theta_int_new)
    
def generateAtomsPosShear (as1,as2,ag1,ag2,am1,am2,n,m,header, Nmaille,c_gr='red',c_s='blue') :
    ''' 
    generates atomic positions for shear strain & writes them to file 
    starting from top and bottom layers parameters
    
    Parameters
    ----------
    as1, as2 : 1D arrays bottom layer periodicity
    ag1, ag2 : 1D arrays top layer periodicity
    am1, am2 : 1D arrays moiré periodicity
    m : commensurate integer for infinite period
    n : commensurate integer for moiré period
    header : name for file
    Nmaille : Commensurability order

    Returns
    -------
    log : information about atomic positions. Also printed.
    Xpt_gr, Ypt_gr : arrays for atomic positions of strained (graphene) layer
    Xpt_s, Ypt_s : arrays for atomic positions of unstrained (support) layer
    f : Gen_files/AtomPos_header.txt contains useful information & Atom pos.
    '''
    log=""
    
    #moire lenghth
    nAm1=(Nmaille*pylab.linalg.norm(am1))
    nAm2=(Nmaille*pylab.linalg.norm(am2))
    if nAm1>nAm2:
        Nx = [-4, int(nAm1+4)]
        Ny = [-4, int(m+4)]
    else:
        Nx = [int(-n)-2, 2]
        Ny = [int(-(m+2)), int(n)+2]
    
    Xpt_gr=[]
    Ypt_gr=[]
    Xpt_s=[]
    Ypt_s=[]
    
    try:
        if header=='Test':
            f = open(os.path.join('Gen_files', 'Test.txt'), "w")
        else:
            f = open(os.path.join('Gen_files', header), "w")
    except IOError:
        f = open("Pos.txt", "w")
        print('Error in opening the usual output file. Positions were saved in ./Pos.txt.')
    f.write("# "+header+"\n")
    f.write("# 2.456 (Ang) = a maille. \t Biplan : m="+str(m)+" n="+str(n)+"\n")
    f.write("#\t"+str(am1[0])+"\t"+str(am1[1])+"\t 0\t vecteur de maille a1 (unite de a)\n")
    f.write("#\t"+str(am2[0])+"\t"+str(am2[1])+"\t 0\t vecteur de maille a2 (unite de a)\n")
    #f.write("#\t"+str(4*(N**2))+"\t Nb atomes dans la maille\n")
    #f.write("#\t 2 \t Nb atomes inequivalents\n")
    f.write("\n")
    f.write("# X\t Y \t Z \t SL\n")
    grA=0
    grB=0
    sA=0
    sB=0

    
    for x in range(Nx[0],Nx[1]):
        for y in range(Ny[0],Ny[1]):
            vecA=x*ag1+y*ag2
            vecB=(x+2./3)*ag1+(y+1./3)*ag2
            pvecA=pproj(vecA,am1,am2)
            pvecB=pproj(vecB,am1,am2)

            if(pvecA[0]>= -0.00001 and pvecA[0]< 0.99999*nAm1 and pvecA[1]>=-0.00001 and pvecA[1]< 0.99999*nAm2):         
                Xpt_gr.append(vecA[0])
                Ypt_gr.append(vecA[1])
                f.write(str(vecA[0])+"\t"+str(vecA[1])+"\t"+str(0.0)+"\t"+"A"+"\n")
                grA=grA+1
            if(pvecB[0]>= -0.00001 and pvecB[0]< 0.99999*nAm1 and pvecB[1]>= -0.00001 and pvecB[1]< 0.99999*nAm2):
                Xpt_gr.append(vecB[0])
                Ypt_gr.append(vecB[1])
                f.write(str(vecB[0])+"\t"+str(vecB[1])+"\t"+str(0.0)+"\t"+"B"+"\n")
                grB=grB+1
    for x in range(Nx[0],Nx[1]):
        for y in range(Ny[0],Ny[1]):
            vecA=x*as1+y*as2
            vecB=(x+2./3)*as1+(y+1./3)*as2
            pvecA=pproj(vecA,am1,am2)
            pvecB=pproj(vecB,am1,am2)

            if(pvecA[0]>= -0.00001 and pvecA[0]< 0.99999*nAm1 and pvecA[1]>= -0.00001 and pvecA[1]< 0.99999*nAm2):         
                Xpt_s.append(vecA[0])
                Ypt_s.append(vecA[1])
                f.write(str(vecA[0])+"\t"+str(vecA[1])+"\t"+str(1.36345)+"\t"+"A"+"\n")
                sA=sA+1
            if(pvecB[0]>= -0.00001 and pvecB[0]< 0.99999*nAm1 and pvecB[1]>= -0.00001 and pvecB[1]< 0.99999*nAm2):
                Xpt_s.append(vecB[0])
                Ypt_s.append(vecB[1])
                f.write(str(vecB[0])+"\t"+str(vecB[1])+"\t"+str(1.36345)+"\t"+"B"+"\n")
                sB=sB+1

    

    #Plot moiré lattice vectors
    pylab.plot([0,am1[0]],[0,am1[1]],color="black")
    pylab.plot([0,am2[0]],[0,am2[1]],color="black")
    pylab.plot([am1[0],am1[0]+am2[0]],[am1[1],am1[1]+am2[1]],color="green")
    pylab.plot([am2[0],am2[0]+am1[0]],[am2[1],am2[1]+am1[1]],color="green")
    #pylab.plot([am1[0],am2[0]],[am1[1],am2[1]],color="black")
    
    #Plot substrate lattice vectors
    pylab.plot([0,as1[0]],[0,as1[1]],color=c_s)
    pylab.plot([0,as2[0]],[0,as2[1]],color=c_s)
    
    #Plot graphene lattice vectors
    pylab.plot([0,ag1[0]],[0,ag1[1]],color=c_gr)    
    pylab.plot([0,ag2[0]],[0,ag2[1]],color=c_gr)


    #Plot atomic positions
    pylab.plot(Xpt_s,Ypt_s,ls='None',marker='o',color=c_s,markersize=5.,markeredgewidth=0.)#,rasterized=True)
    pylab.plot(Xpt_gr,Ypt_gr,ls='None',marker='o',color=c_gr,markersize=2.,markeredgewidth=0.)#,rasterized=True)
    
    

    pylab.axes().set_aspect('equal')
    f.close()
    
    log=log+"\n*** ATOMIC POSITIONS GENERATION ***\n\n"
    log=log+"Number of atoms in:\n"
    log=log+"Sublattice A of gr:"+str(grA)+"\n"
    log=log+"Sublattice B of gr:"+str(grB)+"\n"
    log=log+"Gr plan:"+str(grA+grB)+"\n"
    log=log+"Sublattice A of support:"+str(sA)+"\n"
    log=log+"Sublattice B of support:"+str(sB)+"\n"
    log=log+"Support plan:"+str(sA+sB)+"\n"
    log=log+"Total:"+str(grA+grB+sA+sB)+"\n"

    #insert number of atoms in file
    with open(os.path.join('Gen_files', header), "r") as f:
        contents = f.readlines()
        contents.insert(4, '# '+str(grA+grB+sA+sB))

    with open(os.path.join('Gen_files', header), "w") as f:
        contents = "".join(contents)
        f.write(contents)
    f.close()

    #Return atomic postions and log
    print(log)
    return np.array(Xpt_gr), np.array(Ypt_gr), np.array(Xpt_s), np.array(Ypt_s), log#.encode('utf8')

def calc_ijkl(PM,AM):
    ''' 
    calculates commensurate indexes from PM Matrix
    '''
    a, b, c, d = PM[0,0], PM[0,1], PM[1,0], PM[1,1]
    print('**\n'+str(a) +' '+str(b) +' '+str(c) +' '+str(d) )
    #sur at
    M = (np.linalg.inv(np.array([[1,0], [0,1]])-PM))
    i, j, k, l = M[0,0], M[0,1], M[1,0], M[1,1]
    A = np.dot(M, PM)
    m, n, q, r = A[0,0], A[0,1], A[1,0], A[1,1]
    #sur ab (la même chose)
    # A = np.linalg.inv(np.linalg.inv(PM)-np.array([[1,0], [0,1]]))
    # m, n, q, r = A[0,0], A[0,1], A[1,0], A[1,1]
    # M = np.dot(A, np.linalg.inv(PM))
    # i, j, k, l = M[0,0], M[0,1], M[1,0], M[1,1]

    #Make it a commensurate cell
    ii = round(i)
    jj = round(j)
    kk = round(k)
    ll = round(l)
    mm = round(m)
    nn = round(n)
    qq = round(q)
    rr = round(r)

    N = 1
    iter = 2
    #!!! precision peut être ajustée pour avoir des mailles plus précises
    while ( abs(((1/(ii*ll-kk*jj))*(ll*mm-jj*qq)) -  a) > 0.00001 ) :#and (N<5): #or ( abs(((1/(ii*ll-kk*jj))*(ii*qq-kk*mm)) -  b) > 0.0001 ):#on veut que les quatres decimales soient bonnes
        N=iter
        ii = round(i*N)
        jj = round(j*N)
        kk = round(k*N)
        ll = round(l*N)
        mm = round(m*N)
        nn = round(n*N)
        qq = round(q*N)
        rr = round(r*N)
        iter += 1
    print('\n**commensurate indexes'+str(ii) +' '+str(jj) +' '+str(kk) +' '+str(ll) + str(mm) +' '+str(nn) +' '+str(qq) +' '+str(rr)+'\n')
    print('we find a cell of order' + str(N))

    return(ii,jj,kk,ll,mm,nn,qq,rr,N)  



#%%

def generate_comm_unimoire(thetas, theta_interlayer, p_iso, p_an):
    ''' 
    TODO : test and debug
    function that yields (ijklmnqr) parameters
    that are fit for generating a commensurate cell,
    and corresponding periodicities
    starting from physical strain parameters
    
    Parameters
    ----------
    theta1 : strain direction in degrees
    theta_interlayer : twist angle in degrees
    p_iso : biaxial strain param (float>1)
    p_an : uniaxial strain param (float>1)

    Returns
    -------
    1D commensurate integers
    ab1, ab2 : 1D arrays bottom layer periodicity
    at1_new, at2_new : 1D arrays strained layer periodicity
    am1, am2 : 1D arrays moiré periodicity
    nAm1, nAm2 : float moiré norms
    p_iso, p_an, theta_1, theta_2, theta_int_new :
        physical strain parameters that fit commensurability
        can be slightly dir=fferent from input
    '''
    print('Atom_pos for theta_interlayer=%.1f deg, theta_1=%.1f deg, p_iso=%.4f, p_an=%.4f' % (theta_interlayer, thetas, p_iso, p_an))

    # Passage matrix from xy to hex
    P = np.array([[1., -0.5],[0, 1.0*np.sqrt(3)/2.]], dtype='float32')
    theta1 = -thetas
    theta1b = -np.pi*thetas/180
    theta1b = np.pi*thetas/180
    # Passage matrix from hex to theta (xy if thetas=0)
    #!!! je m'attendais a un changement de signe dans la projection mais apparemment pas besoin. A voir.
    # if thetas<=30:
    B0 = np.array([[np.cos(theta1b), -0.5*np.cos(theta1b)+np.sqrt(3)*0.5*np.sin(theta1b)],
              [-np.sin(theta1b), np.cos(theta1b)*np.sqrt(3)*0.5+0.5*np.sin(theta1b)]], dtype='float32')
    B = np.linalg.inv(B0)
    # else:
    #     B = np.array([[np.cos(theta1b), +0.5*np.cos(theta1b)+np.sqrt(3)*0.5*np.sin(theta1b)],
    #               [np.sin(theta1b), np.cos(theta1b)*np.sqrt(3)*0.5-0.5*np.sin(theta1b)]], dtype='float32')

    # Define basisvectors of bottom graphene in orthogonal basis
    ab1 = np.array([1., 0])
    ab2 = np.array([-1./2, np.sqrt(3)/2])

    # Define initial basisvectors of top graphene in orthogonal basis
    at1 = ab1
    at2 = ab2

    # Find New at1 at2, in the orthogonal basis
    at1_new = np.dot(R(-theta1), np.dot( Eps_u(p_an) , np.dot( R(theta1) , np.dot( Eps_bi(p_iso) , at1))))
    at2_new = np.dot(R(-theta1), np.dot( Eps_u(p_an) , np.dot( R(theta1) , np.dot( Eps_bi(p_iso) , at2))))

    # Find New at1 at2, in the hexagonal basis
    at1_new_hex = np.dot(np.linalg.inv(P), at1_new)
    at2_new_hex = np.dot(np.linalg.inv(P), at2_new)
    # print(at1_new_hex,at2_new_hex)

    # Find New at1 at2, in the rotated orthogonal basis
    at1_new_Otheta = np.dot(np.linalg.inv((B)), at1_new_hex)
    at2_new_Otheta = np.dot(np.linalg.inv((B)), at2_new_hex)
    ab1_Otheta = np.dot(np.linalg.inv((B)), np.array([1,0]))
    ab2_Otheta = np.dot(np.linalg.inv((B)), np.array([0,1]))
    #
    ab1_R = np.dot(np.linalg.inv((B)), np.array([1,0]))
    ab2_R = np.dot(np.linalg.inv((B)), np.array([0,1]))
    at1_new_R = np.dot(Eps_u(p_an), np.array([ab1_R[0], 0]))
    at2_new_R = np.dot(Eps_u(p_an), np.array([ab2_R[0], 0]))
    at1_new_R_hex = np.dot(B, at1_new_R)
    at2_new_R_hex = np.dot(B, at2_new_R)

    # print(at1_new_hex,at2_new_hex)

    print('theta1 applied during transformation = '+str(theta1))

    #New park madden Matrix
    a = at1_new_hex[0]
    b = at1_new_hex[1]
    c = at2_new_hex[0]
    d = at2_new_hex[1]
    PM = np.array([[a,b],[c,d]])

    #go to orthogonal rotated basis
    PMOtheta = np.dot(np.linalg.inv(np.transpose(B0)), np.dot(np.array([[a,b],[c,d]]), np.transpose(B0)))
    # PMOtheta = np.dot(np.linalg.inv(np.array([at1_new_Otheta, at2_new_Otheta])), np.dot(PMOtheta, np.array([ab1_Otheta, ab2_Otheta])))


    #New park madden Matrix Rot
    aR = at1_new_R_hex[0]
    bR = at1_new_R_hex[1]
    cR = at2_new_R_hex[0]
    dR = at2_new_R_hex[1]
    PMR = np.array([[aR,bR],[cR,dR]])
    print(aR,bR,cR,dR)
    
    
    #calculate commensurate indexes
    ii,jj,kk,ll,mm,nn,qq,rr,NN = calc_ijkl(PM,np.array([[1,0],[0,1]]))
    iT,jT,kT,lT,mT,nT,qT,rT,N = calc_ijkl(PMOtheta,np.array([ab1_Otheta, ab2_Otheta]))
    iR,jR,kR,lR,mR,nR,qR,rR,NR = calc_ijkl(PMR,np.array([ab1_R, ab2_R]))
    print(N, NN, NR)

    # Find vectors from abcd Rot
    print('test am1')
    am1R = iR*at1_new_R+jR*at2_new_R
    am2R = kR*at1_new_R+lR*at2_new_R
    nAm1R = (NR*pylab.linalg.norm(am1R))
    nAm2R = (NR*pylab.linalg.norm(am2R))
    print((at1_new_R), (am1R), nAm1R)

    # Find vectors from abcd theta
    ab1 = ab1_Otheta
    ab2 = ab2_Otheta
    # at1_new = at1_new_Otheta
    # at2_new = at2_new_Otheta
    print('test am1')
    am1 = ii*at1_new+jj*at2_new
    am2 = kk*at1_new+ll*at2_new
    nAm1 = (N*pylab.linalg.norm(am1))
    nAm2 = (N*pylab.linalg.norm(am2))
    print((at1_new), (am1), nAm1)
    am1O = iT*at1_new_Otheta+jT*at2_new_Otheta
    am2O = kT*at1_new_Otheta+lT*at2_new_Otheta
    nAm1O = (NN*pylab.linalg.norm(am1O))
    nAm2O = (NN*pylab.linalg.norm(am2O))
    print((at1_new_Otheta), (am1O), nAm1O)

    #calculate strains from abcd hex
    # Find new a,b,c,d
    a = (1/(ii*ll-kk*jj))*(ll*mm-jj*qq)
    b = (1/(ii*ll-kk*jj))*(ll*nn-jj*rr)
    c = (1/(ii*ll-kk*jj))*(ii*qq-kk*mm)
    d = (1/(ii*ll-kk*jj))*(ii*rr-kk*nn)
    print('\n** new a,b,c,d'+str(a)+str(b)+str(c)+str(d))

    # !!! attention à la définition (celle là est physiquement bonne)
    theta_sum = pylab.arctan2(-a + 2*b - 2*c+d, (a+d)*pylab.sqrt(3))
    theta_dif = pylab.arctan2(a + b + 2*c-d, (a-b-d)*pylab.sqrt(3))
    # !!! if theta2 - theta1 < ou > 180, arctg2 ne calcule pas la "vraie" valeur !
    if theta1 >= 90: theta_dif = - 2*np.pi + theta_dif

    theta_2 = 180./pylab.pi * 1./2 * (theta_sum + theta_dif)
    theta_1 = 180./pylab.pi * 1./2 * (theta_sum - theta_dif)
    print('\n****new theta1, theta2')
    print(theta_1, theta_2)

    # is error ok ?
#    DeltaDet = det**2*np.sqrt(((1-d)**2+(1-a)**2+b**2+c**2)*1e-16)
#    DeltaI = i*N*np.sqrt((DeltaDet/det)**2+1e-6/((1-d)**2))
#    print('\nDeltai = ' +str(DeltaI))
    P_1 = a**2 + b**2 - a*b
    DeltaP1 = 1e-5*np.sqrt(5*(a**2 + b**2) - 8*a*b)
    P_2 = c**2 + d**2 - c*d
    DeltaP2 = 1e-5*np.sqrt(5*(c**2 + d**2) - 8*c*d)
    C = 2*(a*c + b*d) - (a*d + b*c)
    DeltaC = 1e-5*np.sqrt(5*(a**2 + b**2+c**2 + d**2) - 8*(a*b+c*d))
    p_iso = pylab.sqrt((2*P_1+2*P_2+C)/3) * pylab.sqrt(1 - pylab.sqrt(1 - 3*(4*P_1*P_2-C**2)/(2*P_1+2*P_2+C)**2))
    if p_iso != p_iso:  # detects situation when p_iso is not a number (argument of sqrt negative)
        p_iso = pylab.sqrt((2*P_1+2*P_2+C)/3) * pylab.sqrt(abs(1 - pylab.sqrt(abs(1 - 3*(4*P_1*P_2-C**2)/(2*P_1+2*P_2+C)**2))))
    p_an  = (2*P_1+2*P_2+C)/pylab.sqrt(3) * 1/pylab.sqrt(4*P_1*P_2-C**2) * (1 + pylab.sqrt(1 - 3*(4*P_1*P_2-C**2)/(2*P_1+2*P_2+C)**2))
    if p_an != p_an:  # detects situation when p_iso is not a number (argument of sqrt negative)
        p_an  = (2*P_1+2*P_2+C)/pylab.sqrt(3) * 1/pylab.sqrt(4*P_1*P_2-C**2) * (abs(1 + pylab.sqrt(abs(1 - 3*(4*P_1*P_2-C**2)/(2*P_1+2*P_2+C)**2))))
    W = 2*P_1+2*P_2+C
    Z = 4*P_1*P_2-C**2
# errors
#    DeltaW = np.sqrt((4./9)*(DeltaP1**2+DeltaP2**2)+(1./9)*DeltaC**2)
#    DeltaZ = np.sqrt(16*(DeltaP1**2*P_2**2+P_1**2*DeltaP2**2)+4*C**2*DeltaC**2)
#    DeltaPiso = np.sqrt( ((-1 + np.sqrt(1 - (3 *Z/W**2)))**2)*DeltaW**2/(4*W*(3 - 3 *np.sqrt(1 - (3* Z/W**2))) *(1 - (3 *Z/W**2)) )  + 3*DeltaZ**2/(16*W**3*(1-3*Z/W**2)*(1-np.sqrt(1-3*Z/W**2)) ) )
#    Deltaepsuni = DeltaPiso*np.sqrt( 1+p_an**2 +p_iso**2)
#    print('*Delta eps_bi = '+str(DeltaPiso))
#    print('*Delta eps_uni = '+str(Deltaepsuni))
    e_bi = p_iso - 1
    e_uni = (p_iso * p_an - p_iso)
    print('eps_uni='+str(e_uni)+', eps_bi ='+str(e_bi))
    if (p_iso > 1 and p_an < 1) or (p_iso < 1 and p_an > 1):
        print("\nNot good!"+"\n")
        p_iso_2 = p_an * p_iso
        p_an_2 = 1 / p_an
        e_bi_2 = p_iso_2 - 1
        e_uni_2 = (p_iso_2 * p_an_2 - p_iso_2)
        if abs(e_bi_2)<abs(e_bi) and abs(e_uni_2) < abs(e_uni):# (abs(e_bi_2) + abs(e_uni_2)) < (abs(e_uni) + abs(e_bi)) : 
            e_bi = e_bi_2; e_uni = e_uni_2
            print('eps_uni='+str(e_uni)+', eps_bi ='+str(e_bi))
            p_iso = p_iso_2
            p_an  = p_an_2
            theta_1 += 90.
            theta_2 -= 90.
            print('p_iso='+str(p_iso)+', p_an ='+str(p_an))
            print('theta1='+str(theta_1)+', theta2 ='+str(theta_2))
        print('eps_uni='+str(e_uni_2)+', eps_bi ='+str(e_bi_2))


    # Find final angle between layers
    alpha_1 = (180./pylab.pi) * pylab.arctan2(b*pylab.sqrt(3), (2*a-b))
    alpha_2 = (180./pylab.pi) * pylab.arctan2(c*pylab.sqrt(3), (c-2*d))
    if alpha_1 < 0.:
        while alpha_1 < 0.: alpha_1 += 60.
    elif alpha_1 > 60.:
        while alpha_1 > 60.: alpha_1 -= 60.
    if alpha_2 < 0.:
        while alpha_2 < 0.: alpha_2 += 60.
    elif alpha_2 > 60.:
        while alpha_2 > 60.: alpha_2 -= 60.
    vec_gr_1 = pylab.array([pylab.sqrt(P_1)*pylab.cos((pylab.pi/180.)*alpha_1), pylab.sqrt(P_1)*pylab.sin((pylab.pi/180.)*alpha_1)])  # a_gr_1 in cartesian coordinates
    vec_gr_2 = pylab.array([pylab.sqrt(P_2)*pylab.cos((pylab.pi/180.)*alpha_2+2*pylab.pi/3), pylab.sqrt(P_2)*pylab.sin((pylab.pi/180.)*alpha_2+2*pylab.pi/3)])  # a_gr_2 in cartesian coordinates
    vec_gr_3 = vec_gr_1 + vec_gr_2 # gives the third direction of graphene
    alpha_3 = (180./pylab.pi) * pylab.arctan2(vec_gr_3[1], vec_gr_3[0]) - 60.
    theta_int_new = (alpha_1 + alpha_2 + alpha_3) / 3.

    return (iR, jR, kR, lR, mR, nR, qR, rR,
            ab1_R, ab2_R, at1_new_R, at2_new_R, am1R, am2R, nAm1R, nAm2R,
            p_iso, p_an, theta_1, theta_2, NR, theta_int_new)


def uniaxial_generateAtomsPos (i,m,j,as1,as2,ag1,ag2,am1,am2,nAm1,nAm2,Nmaille,header,c_gr='red',c_s='blue') :
    ''' 
    TODO : dedug
    generates atomic positions in the uniaxial strain cell & writes them to file
    '''

    log=""
    Xpt_gr=[]
    Ypt_gr=[]
    Xpt_s=[]
    Ypt_s=[]
    
    try:
        if header=='Test':
            f = open(os.path.join('Gen_files', 'Test.txt'), "w")
        else:
            f = open(os.path.join('Gen_files', 'Atom_pos'+header),
                     "w")
    except IOError:
        f = open("Pos.txt", "w")
        print('Error in opening the usual output file. Positions were saved in ./Pos.txt.')
    f.write("# "+header+"\n")
    f.write("#2.46(Ang) = a maille. \t Biplan : i="+str(i)+" j="+str(j)+" m="+str(m)+"\n")
    f.write("#\t"+str(am1[0])+"\t"+str(am1[1])+"\t 0\t vecteur de maille a1 (unite de a)\n")
    f.write("#\t"+str(am2[0])+"\t"+str(am2[1])+"\t 0\t vecteur de maille a2 (unite de a)\n")
    f.write("# X\t Y \t Z \t SL\n")
    grA=0
    grB=0
    sA=0
    sB=0
    
    for x in range(-int(2*Nmaille*(abs(i))),int(2*Nmaille*(abs(i)))):
        for y in range(-int(2*Nmaille*(abs(j))),int(2*Nmaille*(abs(j)))):
            vecA=x*ag1+y*ag2
            vecB=(x+2./3)*ag1+(y+1./3)*ag2
            pvecA=pproj(vecA,am1,am2)
            pvecB=pproj(vecB,am1,am2)
            #if(True):
            if(pvecA[0]>= -0.00001 and pvecA[0]< 0.99999*nAm1 and pvecA[1]>=-0.00001 and pvecA[1]< 0.99999*nAm2):         
                Xpt_gr.append(vecA[0])
                Ypt_gr.append(vecA[1])
                f.write(str(vecA[0])+"\t"+str(vecA[1])+"\t"+str(0.0)+"\t"+"A"+"\n")
                grA=grA+1
            if(pvecB[0]>= -0.00001 and pvecB[0]< 0.99999*nAm1 and pvecB[1]>= -0.00001 and pvecB[1]< 0.99999*nAm2):
                Xpt_gr.append(vecB[0])
                Ypt_gr.append(vecB[1])
                f.write(str(vecB[0])+"\t"+str(vecB[1])+"\t"+str(0.0)+"\t"+"B"+"\n")
                grB=grB+1
    for x in range(-int(2*Nmaille*(abs(m))),int(2*Nmaille*(abs(m)))):
        for y in range(-int(2*Nmaille*(abs(j))),int(2*Nmaille*(abs(j)))):
            vecA=x*as1+y*as2
            vecB=(x+2./3)*as1+(y+1./3)*as2
            pvecA=pproj(vecA,am1,am2)
            pvecB=pproj(vecB,am1,am2)
            #if(True):
            if(pvecA[0]>= -0.00001 and pvecA[0]< 0.99999*nAm1 and pvecA[1]>= -0.00001 and pvecA[1]< 0.99999*nAm2):         
                Xpt_s.append(vecA[0])
                Ypt_s.append(vecA[1])
                f.write(str(vecA[0])+"\t"+str(vecA[1])+"\t"+str(1.36345)+"\t"+"A"+"\n")
                sA=sA+1
            if(pvecB[0]>= -0.00001 and pvecB[0]< 0.99999*nAm1 and pvecB[1]>= -0.00001 and pvecB[1]< 0.99999*nAm2):
                Xpt_s.append(vecB[0])
                Ypt_s.append(vecB[1])
                f.write(str(vecB[0])+"\t"+str(vecB[1])+"\t"+str(1.36345)+"\t"+"B"+"\n")
                sB=sB+1
    #pylab.figure()
    

    #Plot moiré lattice vectors
    pylab.plot([0,am1[0]],[0,am1[1]],color="black")
    pylab.plot([0,am2[0]],[0,am2[1]],color="black")
    pylab.plot([am1[0],am1[0]+am2[0]],[am1[1],am1[1]+am2[1]],color="green")
    pylab.plot([am2[0],am2[0]+am1[0]],[am2[1],am2[1]+am1[1]],color="green")
    #pylab.plot([am1[0],am2[0]],[am1[1],am2[1]],color="black")
    
    #Plot substrate lattice vectors
    pylab.plot([0,as1[0]],[0,as1[1]],color=c_s)
    pylab.plot([0,as2[0]],[0,as2[1]],color=c_s)
    
    #Plot graphene lattice vectors
    pylab.plot([0,ag1[0]],[0,ag1[1]],color=c_gr)    
    pylab.plot([0,ag2[0]],[0,ag2[1]],color=c_gr)


    #Plot atomic positions
    pylab.plot(Xpt_s,Ypt_s,ls='None',marker='o',color=c_s,markersize=10.,markeredgewidth=0.)#,rasterized=True)
    pylab.plot(Xpt_gr,Ypt_gr,ls='None',marker='o',color=c_gr,markersize=5.,markeredgewidth=0.)#,rasterized=True)
    
    

    pylab.axes().set_aspect('equal')
    f.close()
    
    log=log+"\n*** ATOMIC POSITIONS GENERATION ***\n\n"
    log=log+"Number of atoms in:\n"
    log=log+"Sublattice A of gr:"+str(grA)+"\n"
    log=log+"Sublattice B of gr:"+str(grB)+"\n"
    log=log+"Gr plan:"+str(grA+grB)+"\n"
    log=log+"Sublattice A of support:"+str(sA)+"\n"
    log=log+"Sublattice B of support:"+str(sB)+"\n"
    log=log+"Support plan:"+str(sA+sB)+"\n"
    log=log+"Total:"+str(grA+grB+sA+sB)+"\n"
    
    #Return atomic postions and log
    print(log)
    return np.array(Xpt_gr), np.array(Ypt_gr), np.array(Xpt_s), np.array(Ypt_s), log#.encode('utf8')
    

def determine_atoms_fast(i,j,k,l,m,n,q,r,Nmaille,header,c_gr='red',c_s='blue'):
    ''' 
    returns atomic positions faster
    TODO : decrease time to write in file 
    '''

    as1,as2,ag1,ag2,am1,am2,nAm1,nAm2 = generateAtomsVect(i,j,k,l,m,n,q,r,Nmaille)

    log=""
    
    try:
        if header=='Test':
            f = open(os.path.join('Gen_files', 'Test.txt'), "w")
        else:
            f = open(os.path.join('Gen_files', 'Atom_pos'+header),
                     "w")
    except IOError:
        f = open("Pos.txt", "w")
        print('Error in opening the usual output file. Positions were saved in ./Pos.txt.')
    f.write("# "+header+"\n")
    f.write("#2.46(Ang) = a maille. \t Biplan : i="+str(i)+" j="+str(j)+" k="+str(k)+" l="+str(l)+" m="+str(m)+" n="+str(n)+" q="+str(q)+" r="+str(r)+"\n")
    f.write("#\t"+str(am1[0])+"\t"+str(am1[1])+"\t 0\t vecteur de maille a1 (unite de a)\n")
    f.write("#\t"+str(am2[0])+"\t"+str(am2[1])+"\t 0\t vecteur de maille a2 (unite de a)\n")
    #f.write("#\t"+str(4*(N**2))+"\t Nb atomes dans la maille\n")
    #f.write("#\t 2 \t Nb atomes inequivalents\n")
    f.write("# X\t Y \t Z \t SL\n")

    nAm1 = (Nmaille*pylab.linalg.norm(am1))
    nAm2 = (Nmaille*pylab.linalg.norm(am2))
  
    x = np.array([i for i in range(-int(Nmaille*(abs(i)+abs(k))),
                                   int(Nmaille*(abs(i)+abs(k))))])
    y = np.array([i for i in range(-int(Nmaille*(abs(j)+abs(l))),
                                   int(Nmaille*(abs(j)+abs(l))))])
    X_gAt = np.concatenate([y*ag2[0]+i*ag1[0] for i in x ], axis=0)
    X_gBt = np.concatenate([(y+1./3)*ag2[0]+(i+2./3)*ag1[0] for i in x ], axis=0)
    Y_gAt = np.concatenate([y*ag2[1]+i*ag1[1] for i in x ], axis=0)
    Y_gBt = np.concatenate([(y+1./3)*ag2[1]+(i+2./3)*ag1[1] for i in x ], axis=0)

    xs = np.array([i for i in range(-int(Nmaille*(abs(m)+abs(q))),int(Nmaille*(abs(m)+abs(q))))])
    ys = np.array([i for i in range(-int(Nmaille*(abs(n)+abs(r))),int(Nmaille*(abs(n)+abs(r))))])
    X_sAt = np.concatenate([ys*as2[0]+i*as1[0] for i in xs ], axis=0)
    X_sBt = np.concatenate([(ys+1./3)*as2[0]+(i+2./3)*as1[0] for i in x ], axis=0)
    Y_sAt = np.concatenate([ys*as2[1]+i*as1[1] for i in xs ], axis=0)
    Y_sBt = np.concatenate([(ys+1./3)*as2[1]+(i+2./3)*as1[1] for i in xs ], axis=0)
    print(len(X_gAt), len(Y_gAt), len(X_sAt), len(Y_sAt), len(X_gBt), len(Y_gBt), len(X_sBt), len(Y_sBt))
    PgA = np.array([pproj([X_gAt[i], Y_gAt[i]],am1,am2) for i in range(min(len(X_gAt), len(Y_gAt)))])
    PgB = np.array([pproj([X_gBt[i], Y_gBt[i]],am1,am2) for i in range(min(len(X_gBt), len(Y_gBt)))])
    PsA = np.array([pproj([X_sAt[i], Y_sAt[i]],am1,am2) for i in range(min(len(X_sAt), len(Y_sAt)))])
    PsB = np.array([pproj([X_sBt[i], Y_sBt[i]],am1,am2) for i in range(min(len(X_sBt), len(Y_sBt)))])
    X_gA = np.array([X_gAt[i] for i in range(len(X_gAt)) if (PgA[i][0]>= -0.00001 
            and PgA[i][0]< 0.99999*nAm1 
            and PgA[i][1]>=-0.00001 
            and PgA[i][1]< 0.99999*nAm2)])
    Y_gA = np.array([Y_gAt[i] for i in range(len(Y_gAt)) if (PgA[i][0]>= -0.00001 
            and PgA[i][0]< 0.99999*nAm1 
            and PgA[i][1]>=-0.00001 
            and PgA[i][1]< 0.99999*nAm2)])
    X_sA = np.array([X_sAt[i] for i in range(len(X_sAt)) if (PsA[i][0]>= -0.00001 
            and PsA[i][0]< 0.99999*nAm1 
            and PsA[i][1]>=-0.00001 
            and PsA[i][1]< 0.99999*nAm2)])
    Y_sA = np.array([Y_sAt[i] for i in range(len(Y_sAt)) if (PsA[i][0]>= -0.00001 
            and PsA[i][0]< 0.99999*nAm1 
            and PsA[i][1]>=-0.00001 
            and PsA[i][1]< 0.99999*nAm2)])
    X_gB = np.array([X_gBt[i] for i in range(len(X_gBt)) if (PgB[i][0]>= -0.00001 
            and PgB[i][0]< 0.99999*nAm1 
            and PgB[i][1]>=-0.00001 
            and PgB[i][1]< 0.99999*nAm2)])
    Y_gB = np.array([Y_gBt[i] for i in range(len(Y_gBt)) if (PgB[i][0]>= -0.00001 
            and PgB[i][0]< 0.99999*nAm1 
            and PgB[i][1]>=-0.00001 
            and PgB[i][1]< 0.99999*nAm2)])
    X_sB = np.array([X_sBt[i] for i in range(len(X_sBt)) if (PsB[i][0]>= -0.00001 
            and PsB[i][0]< 0.99999*nAm1 
            and PsB[i][1]>=-0.00001 
            and PsB[i][1]< 0.99999*nAm2)])
    Y_sB = np.array([Y_sBt[i] for i in range (len(Y_sBt)) if (PsB[i][0]>= -0.00001 
            and PsB[i][0]< 0.99999*nAm1 
            and PsB[i][1]>=-0.00001 
            and PsB[i][1]< 0.99999*nAm2)])

    for index in range(len(X_gA)):
        f.write(str(X_gA[index])+"\t"+str(Y_gA[index])+"\t"+str(0.0)+"\t"+"A"+"\n"
                +str(X_gB[index])+"\t"+str(Y_gB[index])+"\t"+str(0.0)+"\t"+"B"+"\n")
    for index in range(len(X_sAt)):       
        f.write(str(X_sA[index])+"\t"+str(Y_sA[index])+"\t"+str(1.36345)+"\t"+"A"+"\n"
                +str(X_sB[index])+"\t"+str(Y_sB[index])+"\t"+str(1.36345)+"\t"+"B"+"\n")
    # f.writelines(['%f\t %f \t %f \t %i \n' %( item  for item in list])

    #Plot moiré lattice vectors
    pylab.plot([0,am1[0]],[0,am1[1]],color="black")
    pylab.plot([0,am2[0]],[0,am2[1]],color="black")
    pylab.plot([am1[0],am1[0]+am2[0]],[am1[1],am1[1]+am2[1]],color="green")
    pylab.plot([am2[0],am2[0]+am1[0]],[am2[1],am2[1]+am1[1]],color="green")
    #pylab.plot([am1[0],am2[0]],[am1[1],am2[1]],color="black")
    
    #Plot substrate lattice vectors
    pylab.plot([0,as1[0]],[0,as1[1]],color=c_s)
    pylab.plot([0,as2[0]],[0,as2[1]],color=c_s)
    
    #Plot graphene lattice vectors
    pylab.plot([0,ag1[0]],[0,ag1[1]],color=c_gr)    
    pylab.plot([0,ag2[0]],[0,ag2[1]],color=c_gr)


    #Plot atomic positions
    Xpt_gr = np.concatenate([X_gA, X_gB])
    Ypt_gr = np.concatenate([Y_gA, Y_gB])
    Xpt_s = np.concatenate([X_sA, X_sB])
    Ypt_s = np.concatenate([Y_sA, Y_sB])

    pylab.plot(Xpt_s,Ypt_s,ls='None',marker='o',color=c_s,markersize=2.,markeredgewidth=0.)#,rasterized=True)
    pylab.plot(Xpt_gr,Ypt_gr,ls='None',marker='o',color=c_gr,markersize=2.,markeredgewidth=0.)#,rasterized=True)


    pylab.axes().set_aspect('equal')
    f.close()
    
    log=log+"\n*** ATOMIC POSITIONS GENERATION ***\n\n"
    log=log+"Number of atoms in:\n"
    log=log+"Sublattice A of gr:"+str(len(X_gA))+"\n"
    log=log+"Sublattice B of gr:"+str(len(X_gB))+"\n"
    log=log+"Gr plan:"+str(len(X_gA)+len(X_gB))+"\n"
    log=log+"Sublattice A of support:"+str(len(X_sA))+"\n"
    log=log+"Sublattice B of support:"+str(len(X_sB))+"\n"
    log=log+"Support plan:"+str(len(X_sB)+len(X_sA))+"\n"
    log=log+"Total:"+str(len(X_sA)+len(X_sB)+len(X_gA)+len(X_gB))+"\n"
    
    #Return atomic postions and log
    print(log)
    return np.array(Xpt_gr), np.array(Ypt_gr), np.array(Xpt_s), np.array(Ypt_s), log#.encode('utf8')


def generateAtomsPos3Layers (i,j,k,l,i2,j2,k2,l2,m,n,q,r,Nmaille,header,c_gr='g',c_s='blue',c_gr2='r') :
    ''' TODO : tests
    Used once, on 3 layer system @ P et JY
    '''

    as1,as2,ag1,ag2,am1,am2,nAm1,nAm2 = generateAtomsVect(i,j,k,l,m,n,q,r,Nmaille)
    as1,as2,ag1_2,ag2_2,am1_2,am2_2,nAm1_2,nAm2_2 = generateAtomsVect(i2,j2,k2,l2,m,n,q,r,Nmaille)

    log=""

    Xpt_gr=[]
    Ypt_gr=[]
    Xpt_s=[]
    Ypt_s=[]
    Xpt_gr2=[]
    Ypt_gr2=[]
    
    try:
        if header=='Test':
            f = open(os.path.join('Gen_files', 'Test.txt'), "w")
        else:
            f = open(os.path.join('Gen_files', 'Atom_pos'+header),
                     "w")
    except IOError:
        f = open("Pos.txt", "w")
        print('Error in opening the usual output file. Positions were saved in ./Pos.txt.')
    f.write("# "+header+"\n")
    f.write("#2.46(Ang) = a maille. \t Biplan : i="+str(i)+" j="+str(j)+" k="+str(k)+" l="+str(l)+" i2="+str(i2)+" j2="+str(j2)+" k2="+str(k2)+" l2="+str(l2)+" m="+str(m)+" n="+str(n)+" q="+str(q)+" r="+str(r)+"\n")
    f.write("#\t"+str(am1[0])+"\t"+str(am1[1])+"\t 0\t vecteur de maille a1 (unite de a)\n")
    f.write("#\t"+str(am2[0])+"\t"+str(am2[1])+"\t 0\t vecteur de maille a2 (unite de a)\n")
    f.write("#\t"+str(am1_2[0])+"\t"+str(am1_2[1])+"\t z\t vecteur de maille a1 (unite de a)\n")
    f.write("#\t"+str(am2_2[0])+"\t"+str(am2_2[1])+"\t z\t vecteur de maille a2 (unite de a)\n")
    #f.write("#\t"+str(4*(N**2))+"\t Nb atomes dans la maille\n")
    #f.write("#\t 2 \t Nb atomes inequivalents\n")
    f.write("# X\t Y \t Z \t SL\n")
    grA=0
    grB=0
    sA=0
    sB=0
    grA2=0
    grB2=0

    for x in range(-int(Nmaille*(abs(i)+abs(k))),int(Nmaille*(abs(i)+abs(k)))):
        for y in range(-int(Nmaille*(abs(j)+abs(l))),int(Nmaille*(abs(j)+abs(l)))):
            vecA=x*ag1+y*ag2
            vecB=(x+2./3)*ag1+(y+1./3)*ag2
            pvecA=pproj(vecA,am1,am2)
            pvecB=pproj(vecB,am1,am2)
            #if(True):
            if(pvecA[0]>= -0.00001 and pvecA[0]< 0.99999*nAm1 and pvecA[1]>=-0.00001 and pvecA[1]< 0.99999*nAm2):         
                Xpt_gr.append(vecA[0])
                Ypt_gr.append(vecA[1])
                f.write(str(vecA[0])+"\t"+str(vecA[1])+"\t"+str(0.0)+"\t"+"A"+"\n")
                grA=grA+1
            if(pvecB[0]>= -0.00001 and pvecB[0]< 0.99999*nAm1 and pvecB[1]>= -0.00001 and pvecB[1]< 0.99999*nAm2):
                Xpt_gr.append(vecB[0])
                Ypt_gr.append(vecB[1])
                f.write(str(vecB[0])+"\t"+str(vecB[1])+"\t"+str(0.0)+"\t"+"B"+"\n")
                grB=grB+1
    for x in range(-int(Nmaille*(abs(m)+abs(q))),int(Nmaille*(abs(m)+abs(q)))):
        for y in range(-int(Nmaille*(abs(n)+abs(r))),int(Nmaille*(abs(n)+abs(r)))):
            vecA=x*as1+y*as2
            vecB=(x+2./3)*as1+(y+1./3)*as2
            pvecA=pproj(vecA,am1,am2)
            pvecB=pproj(vecB,am1,am2)
            #if(True):
            if(pvecA[0]>= -0.00001 and pvecA[0]< 0.99999*nAm1 and pvecA[1]>= -0.00001 and pvecA[1]< 0.99999*nAm2):         
                Xpt_s.append(vecA[0])
                Ypt_s.append(vecA[1])
                f.write(str(vecA[0])+"\t"+str(vecA[1])+"\t"+str(1.36345)+"\t"+"A"+"\n")
                sA=sA+1
            if(pvecB[0]>= -0.00001 and pvecB[0]< 0.99999*nAm1 and pvecB[1]>= -0.00001 and pvecB[1]< 0.99999*nAm2):
                Xpt_s.append(vecB[0])
                Ypt_s.append(vecB[1])
                f.write(str(vecB[0])+"\t"+str(vecB[1])+"\t"+str(1.36345)+"\t"+"B"+"\n")
                sB=sB+1
    for x in range(-int(2*Nmaille*(abs(i)+abs(k))),int(2*Nmaille*(abs(i)+abs(k)))):
        for y in range(-int(2*Nmaille*(abs(j)+abs(l))),int(2*Nmaille*(abs(j)+abs(l)))):
            vecA=x*ag1_2+y*ag2_2
            vecB=(x+2./3)*ag1_2+(y+1./3)*ag2_2
            pvecA=pproj(vecA,am1_2,am2_2)
            pvecB=pproj(vecB,am1_2,am2_2)
            #if(True):
            if(pvecA[0]>= -0.00001 and pvecA[0]< 0.99999*nAm1_2 and pvecA[1]>=-0.00001 and pvecA[1]< 0.99999*nAm2_2):         
                Xpt_gr2.append(vecA[0])
                Ypt_gr2.append(vecA[1])
                f.write(str(vecA[0])+"\t"+str(vecA[1])+"\t"+str(2.7269)+"\t"+"A"+"\n")
                grA2=grA2+1
            if(pvecB[0]>= -0.00001 and pvecB[0]< 0.99999*nAm1_2 and pvecB[1]>= -0.00001 and pvecB[1]< 0.99999*nAm2_2):
                Xpt_gr2.append(vecB[0])
                Ypt_gr2.append(vecB[1])
                f.write(str(vecB[0])+"\t"+str(vecB[1])+"\t"+str(2.7269)+"\t"+"B"+"\n")
                grB2=grB2+1
    #pylab.figure()
    

    #Plot moiré lattice vectors
    pylab.plot([0,am1[0]],[0,am1[1]],color="black")
    pylab.plot([0,am2[0]],[0,am2[1]],color="black")
    pylab.plot([am1[0],am1[0]+am2[0]],[am1[1],am1[1]+am2[1]],color="green")
    pylab.plot([am2[0],am2[0]+am1[0]],[am2[1],am2[1]+am1[1]],color="green")
    pylab.plot([0,am1_2[0]],[0,am1_2[1]],color="purple")
    pylab.plot([0,am2_2[0]],[0,am2_2[1]],color="r")
    pylab.plot([am1_2[0],am1_2[0]+am2_2[0]],[am1_2[1],am1_2[1]+am2_2[1]],color="purple")
    pylab.plot([am2_2[0],am2_2[0]+am1_2[0]],[am2_2[1],am2_2[1]+am1_2[1]],color="r")
    #pylab.plot([am1[0],am2[0]],[am1[1],am2[1]],color="black")
    
    #Plot substrate lattice vectors
    pylab.plot([0,as1[0]],[0,as1[1]],color=c_s)
    pylab.plot([0,as2[0]],[0,as2[1]],color=c_s)
    
    #Plot graphene lattice vectors
    pylab.plot([0,ag1[0]],[0,ag1[1]],color=c_gr)    
    pylab.plot([0,ag2[0]],[0,ag2[1]],color=c_gr)
    pylab.plot([0,ag1_2[0]],[0,ag1_2[1]],color=c_gr2)    
    pylab.plot([0,ag2_2[0]],[0,ag2_2[1]],color=c_gr2)


    #Plot atomic positions
    pylab.plot(Xpt_s,Ypt_s,ls='None',marker='o',color=c_s,markersize=2.,markeredgewidth=0.)#,rasterized=True)
    pylab.plot(Xpt_gr,Ypt_gr,ls='None',marker='o',color=c_gr,markersize=2.,markeredgewidth=0.)#,rasterized=True)
    pylab.plot(Xpt_gr2,Ypt_gr2,ls='None',marker='o',color=c_gr2,markersize=2.,markeredgewidth=0.)#,rasterized=True)
    
    

    pylab.axes().set_aspect('equal')
    f.close()
    
    log=log+"\n*** ATOMIC POSITIONS GENERATION ***\n\n"
    log=log+"Number of atoms in:\n"
    log=log+"Sublattice A of gr bottom:"+str(grA)+"\n"
    log=log+"Sublattice B of gr bottom:"+str(grB)+"\n"
    log=log+"Gr plan:"+str(grA+grB)+"\n"
    log=log+"Sublattice A of support:"+str(sA)+"\n"
    log=log+"Sublattice B of support:"+str(sB)+"\n"
    log=log+"Support plan:"+str(sA+sB)+"\n"
    log=log+"Total:"+str(grA+grB+sA+sB)+"\n"
    log=log+"Number of atoms in:\n"
    log=log+"Sublattice A of gr2 top:"+str(grA2)+"\n"
    log=log+"Sublattice B of gr2 top:"+str(grB2)+"\n"
    log=log+"Gr plan:"+str(grA2+grB2)+"\n"
    #Return atomic postions and log
    print(log)
    return np.array(Xpt_gr), np.array(Ypt_gr), np.array(Xpt_gr2), np.array(Ypt_gr2), np.array(Xpt_s), np.array(Ypt_s), log#.encode('utf8')
