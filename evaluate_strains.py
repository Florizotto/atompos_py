#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Functions related to the evaluation of strain in a cell

"""
import numpy as np
import pylab
import constants as const

def is_error(p_an, p_iso, det, a, b, c, d, i, N,
             DeltaP1, DeltaP2, DeltaC, P_1, P_2, C, Z, W):
    DeltaDet = det**2*np.sqrt(((1-d)**2+(1-a)**2+b**2+c**2)*1e-16)
    DeltaI = i*N*np.sqrt((DeltaDet/det)**2+1e-6/((1-d)**2))
    print('\nDeltai = ' +str(DeltaI))
    DeltaW = np.sqrt((4./9)*(DeltaP1**2+DeltaP2**2)+(1./9)*DeltaC**2)
    DeltaZ = np.sqrt(16*(DeltaP1**2*P_2**2+P_1**2*DeltaP2**2)+4*C**2*DeltaC**2)
    DeltaPiso = np.sqrt( ((-1 + np.sqrt(1 - (3 *Z/W**2)))**2)*DeltaW**2/(4*W*(3 - 3 *np.sqrt(1 - (3* Z/W**2))) *(1 - (3 *Z/W**2)) )  + 3*DeltaZ**2/(16*W**3*(1-3*Z/W**2)*(1-np.sqrt(1-3*Z/W**2)) ) )
    Deltaepsuni = DeltaPiso*np.sqrt( 1+p_an**2 +p_iso**2)
    print('*Delta eps_bi = '+str(DeltaPiso))
    print('*Delta eps_uni = '+str(Deltaepsuni))
    

def calc_p1p2alpha12 (a,b,c,d):
    p_1 = pylab.sqrt(a**2+b**2-a*b)
    p_2 = pylab.sqrt(c**2+d**2-c*d)
    alpha_1 = (180./pylab.pi) * pylab.arctan2(b*pylab.sqrt(3),(2*a-b))
    alpha_2 = (180./pylab.pi) * pylab.arctan2(c*pylab.sqrt(3),(c-2*d))
    if alpha_1 < 0.:
        while alpha_1 < 0.: alpha_1 += 60.
    elif alpha_1 > 60.:
        while alpha_1 > 60.: alpha_1 -= 60.
    if alpha_2 < 0.:
        while alpha_2 < 0.: alpha_2 += 60.
    elif alpha_2 > 60.:
        while alpha_2 > 60.: alpha_2 -= 60.
        
    return (p_1,p_2,alpha_1,alpha_2)

def number_moirons_func(coordinates,a,b,c,d):
    m,n,i,j = coordinates[0],coordinates[1],coordinates[2],coordinates[3]
    return (a*i+b*j+c*m+d*n)**2 + (a*j-b*(i-j)+c*n-d*(m-n))**2 + (a*i+b*j+c*m+d*n)*(-a*j+b*(i-j)-c*n+d*(m-n))

def calculate_woods(substrate,i,j,k,l,m,n,q,r,space,MoireGrid=False):
    try:
        a_TM=const.lattice_params[substrate]
    except KeyError:
        print("\nUnknown metal. Typo?")
        a_TM = 0.2706
    log=""
    i = float(i)
    j = float(j)
    k = float(k)
    l = float(l)
    m = float(m)
    n = float(n)
    q = float(q)
    r = float(r)
    # Build the matrices in the chosen space:
    if space == "dir":
        MRe_matrix = pylab.array([[i,j],[k,l]])
        MGr_matrix = pylab.array([[m,n],[q,r]])
        a = (1/(i*l-k*j))*(l*m-j*q)
        b = (1/(i*l-k*j))*(l*n-j*r)
        c = (1/(i*l-k*j))*(i*q-k*m)
        d = (1/(i*l-k*j))*(i*r-k*n)        
        GrRe_matrix= pylab.array([[a,b],[c,d]])
    elif space == "rec":
        MRe_matrix = pylab.array([[i,k],[j,l]])
        MGr_matrix = pylab.array([[m,q],[n,r]])
        a = (1/(i*l-k*j))*(l*m-k*n)
        b = (1/(i*l-k*j))*(l*q-k*r)
        c = (1/(i*l-k*j))*(i*n-j*m)
        d = (1/(i*l-k*j))*(i*r-j*q)        
        GrRe_matrix= pylab.array([[a,b],[c,d]])
    
    p_1,p_2,alpha_1,alpha_2 = calc_p1p2alpha12(a,b,c,d)
    
    log=log+"\n*** GENERALIZED WOODS ANALYSIS ***\n"
    log=log+"\nFor graphene with respect to "+substrate+":\n"
    log=log+"(p_1,p_2,alpha_1,alpha_2) = "+str((round(p_1,5),round(p_2,5),round(alpha_1,3),round(alpha_2,3)))+"\n"
#    vec_gr_1 = pylab.array([p_1*(pylab.cos(alpha_1)+pylab.sin(alpha_1)/pylab.sqrt(3)),p_1*(2*pylab.sin(alpha_1)/pylab.sqrt(3))]) # a_gr_1 in hexagonal coordinates
#    vec_gr_2 = pylab.array([p_2*(-2*pylab.sin(alpha_2)/pylab.sqrt(3)),                p_2*(pylab.cos(alpha_2)-pylab.sin(alpha_2)/pylab.sqrt(3))]) # a_gr_2 in hexagonal coordinates
    vec_gr_1 = pylab.array([p_1*pylab.cos((pylab.pi/180.)*alpha_1             ),p_1*pylab.sin((pylab.pi/180.)*alpha_1             )]) # a_gr_1 in cartesian coordinates
    vec_gr_2 = pylab.array([p_2*pylab.cos((pylab.pi/180.)*alpha_2+2*pylab.pi/3),p_2*pylab.sin((pylab.pi/180.)*alpha_2+2*pylab.pi/3)]) # a_gr_2 in cartesian coordinates
    vec_gr_3 = vec_gr_1 + vec_gr_2 # gives the third direction of graphene
    #Norms in angströms
    a_gr_1 = p_1*10*a_TM
    a_gr_2 = p_2*10*a_TM
    a_gr_3 = pylab.norm(vec_gr_3)*10*a_TM
    alpha_3 = (180./pylab.pi) * pylab.arctan2(vec_gr_3[1],vec_gr_3[0]) - 60.
    log=log+"Graphene then has 3 lattice parameters: "+str(round(a_gr_1,5))+u"\u00c5 , "+str(round(a_gr_2,5))+u"\u00c5 , "+str(round(a_gr_3,5))+u"\u00c5.\n"
    log=log+"and 3 angles with respect to "+substrate+": "+str(round(alpha_1,3))+u"° , "+str(round(alpha_2,3))+u"° , "+str(round(alpha_3,3))+u"°.\n"
    log=log+"On average, this makes: a_gr = "+str(round((a_gr_1+a_gr_2+a_gr_3)/3.,5))+u"\u00c5, and phi = "+str(round((alpha_1+alpha_2+alpha_3)/3.,3))+u"°.\n"

    p_ss_1 = pylab.sqrt(m**2+n**2-m*n) # scaling factor for the superstructure
    p_ss_2 = pylab.sqrt(q**2+r**2-q*r)
    number_moirons_1 = number_moirons_func([m,n,i,j],1,0,-1,0)
    number_moirons_2 = number_moirons_func([q,r,k,l],1,0,-1,0)
    p_m_1 = p_ss_1/pylab.sqrt(number_moirons_1) # scaling factor for the moiré cell
    p_m_2 = p_ss_2/pylab.sqrt(number_moirons_2) # scaling factor for the moiré cell in the other direction
    alpha_m_1 = (180./pylab.pi) * pylab.arctan(n*pylab.sqrt(3)/(2*m-n))
    alpha_m_2 = (180./pylab.pi) * pylab.arctan(q*pylab.sqrt(3)/(q-2*r))
    if alpha_m_1 < 0.:
        while alpha_m_1 < 0.: alpha_m_1 += 60.
    elif alpha_m_1 > 60.:
        while alpha_m_1 > 60.: alpha_m_1 -= 60.
    if alpha_m_2 < 0.:
        while alpha_m_2 < 0.: alpha_m_2 += 60.
    elif alpha_m_2 > 60.:
        while alpha_m_2 > 60.: alpha_m_2 -= 60.
    log=log+"The number of beatings (moirons) is of "+str(pylab.sqrt(number_moirons_1))+" along the first direction and of "+str(pylab.sqrt(number_moirons_2))+" along the second direction.\n"
    log=log+"For moir"+u"é"+" with respect to "+substrate+":\n"
    log=log+"(p_1,p_2,alpha_1,alpha_2) = "+str((round(p_m_1,5),round(p_m_2,5),round(alpha_m_1,3),round(alpha_m_2,3)))+"\n"
    vec_m_1 = pylab.array([p_m_1*pylab.cos((pylab.pi/180.)*alpha_m_1             ),p_m_1*pylab.sin((pylab.pi/180.)*alpha_m_1             )]) # a_m_1 in cartesian coordinates
    vec_m_2 = pylab.array([p_m_2*pylab.cos((pylab.pi/180.)*alpha_m_2+2*pylab.pi/3),p_m_2*pylab.sin((pylab.pi/180.)*alpha_m_2+2*pylab.pi/3)]) # a_m_2 in cartesian coordinates
    vec_m_3 = vec_m_1 + vec_m_2 # gives the third direction of moiré
    a_m_1 = p_m_1*a_TM
    a_m_2 = p_m_2*a_TM
    a_m_3 = pylab.norm(vec_m_3)*a_TM
    alpha_m_3 = (180./pylab.pi) * pylab.arctan2(vec_m_3[1],vec_m_3[0]) - 60.
    log=log+"The moir"+u"é"+" then has 3 lattice parameters: "+str(round(a_m_1,3))+"nm , "+str(round(a_m_2,3))+"nm , "+str(round(a_m_3,3))+"nm.\n"
    log=log+"and 3 angles with respect to "+substrate+": "+str(round(alpha_m_1,3))+u"° , "+str(round(alpha_m_2,3))+u"° , "+str(round(alpha_m_3,3))+u"°.\n"
    log=log+"On average, this makes: a_m = "+str(round((a_m_1+a_m_2+a_m_3)/3.,5))+" nm, and phi = "+str(round((alpha_m_1+alpha_m_2+alpha_m_3)/3.,3))+u"°.\n"

    print(log)
    if MoireGrid:
        return log#.encode('utf8')
    else:
        return log, round(a_m_1,3), round(a_m_2,3), round(a_m_3,3), round(alpha_m_1,3), round(alpha_m_2,3)#.encode('utf8')
