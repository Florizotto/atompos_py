# -*- coding: utf-8 -*-
"""
constants

Defines physical constants in SI units for easy computation
Values from Particle Physics Booklet (July 2012)
DANGER : may overwrite existing variables if imported as *

"""


#Speed of light
c=299792458
#Planck's constant and its reduced form
h=6.62606957*10**(-34)
hbar = 1.0545718*10**(-34)  # J.s
#Electronic charge
e=1.602176565*10**(-19)
#Boltzmann's constant
kb=1.3806488*10**(-23)
#Fermi velocity of graphene
vf=1.1*10**(6)
#with Guy's params
vF = 1.09*10**15  # nm/s
dtdr = 3.7*22.18*1.6021766208*10**-19 # unit : J.nm-1

#Lattice parameters of usual substrates
# Cu from Straumanis1969
# gr predicted by Zakharchenko2009
lattice_params={'Re':0.27609,
                'Ir':0.27147,
                'Pt':0.27744,
                'Ru':0.27058,
                'Cu':0.25558,
                'Ni':0.24918,
                'HOPG':0.24612,
                'gr':0.2456}