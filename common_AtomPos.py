#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Common functions

"""
import numpy as np
import pylab
import os.path


def Rotation(theta):
    return pylab.array([[pylab.cos(theta),-pylab.sin(theta)],
                        [pylab.sin(theta), pylab.cos(theta)]])
                       
def pproj(pos, u1, u2):
    # Calculates the coordinates of the vector position pos in the moiré cell:
    u1 = u1/pylab.linalg.norm(u1)
    u2 = u2/pylab.linalg.norm(u2)
    rot = Rotation(pylab.pi/2)
    return pylab.array([pylab.dot(pylab.dot(rot,u2),pos)/pylab.dot(pylab.dot(rot,u2),u1),
                        pylab.dot(pylab.dot(rot,u1),pos)/pylab.dot(pylab.dot(rot,u1),u2)])

def R(angle): #rotation matrix (angle en degres)
    angle = angle*np.pi/180. #en rad
    R=np.array([[np.cos(angle),-np.sin(angle)],[np.sin(angle),np.cos(angle)]], dtype='float32')
    return R

def Eps_u(p_an):
    Epsu=np.array([[p_an,0],[0,1]])
    return Epsu


def Eps_bi(p_iso):
    Epsbi=np.array([[p_iso,0],[0,p_iso]])
    return Epsbi


def lines_that_contain(header, string1, string2):
    with open(os.path.join('Gen_files', header)) as fp:
        print('\n')
        i=0
        for line in fp.readlines():
            i+=1
            line_elements = line.split()
            if string1 == line_elements[0] and  string2 == line_elements[1] :
                return (i-6+1, line_elements[-1])
        return('no match')


def findClosestValue(xarray,xvalue_to_find,yarray,yvalue_to_find):
    min_ecart=0
    for i in range(0,len(xarray)):
        current_mismatch=abs(xarray[i]-xvalue_to_find)+abs(yarray[i]-yvalue_to_find)
        if(current_mismatch<min_ecart or i==0):
            min_ecart=current_mismatch
            xvalue_found=xarray[i]
            yvalue_found=yarray[i]
    return xvalue_found, yvalue_found

def getatompos(event, gx, gy, header):
    xevent = event.xdata
    #print(xevent)
    yevent = event.ydata
    #print(yevent)
    xx,yy = findClosestValue(gx, xevent, gy, yevent)
    nbre,typeatome = lines_that_contain(header, str(xx),str(yy))
    print('atome'+str(nbre))
    print('X_gr='+str(xx)+'; Y_gr='+str(yy))
    print('atome'+str(typeatome))
    