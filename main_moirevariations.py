#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  9 09:38:35 2019

@author: florie
"""
from __future__ import print_function
import pylab
import numpy as np
import matplotlib.pyplot as plt
from common_AtomPos import getatompos
from generate import generate_comm, generateAtomsPos
from evaluate_strains import calculate_woods
import constants as const 

figure = pylab.figure()
plt.style.use('/home/florie/Documents/LABO/Python/nature.mplstyle') #pour avoir les jolies ecritures

cid = figure.canvas.mpl_connect('button_press_event', lambda event: getatompos(event, gx, gy, header))    


#%%
# Save log ?
log = True
log1 = ""
log2 = ""
log3 = ""

# Loic PRL Data :
# (p_iso,p_an) = (0.9988466529947277, 1.0037537872834086)
# (theta_1,theta_2) = (-19.283880872421673, 20.542548943411866)
# (e_bi,e_uni) = (-0.0011533470052722539, 0.003749457864086869)


theta1_applied = []
theta2_applied = []
theta1_calculated = []
p_an_calc = []
p_iso_calc = []
Nm = []
theta_int_calc = []
lam = []
phiam = []

for index in [-54]: #range(41*18, 44*18, 1):#[30*8]:  # range(20*8, 40*8, 1):
    i = index/(1.)
    if i == 90 : continue
    print(i)
    epshbn = i/100
    a = const.lattice_params['gr']
    thetahbn = np.arccos(1-(1/(2*(1+epshbn)))*((a*(1+epshbn)/15.8)**2-epshbn**2))*180/np.pi
    theta1, theta_interlayer, p_iso_in, p_an_in = i, 0.059, 1-0.0001, 1.000-0.00057 #1.0-0.0008, 1-0.00057
    theta1_applied.append(i)
    i,j,k,l,m,n,q,r,ab1,ab2,at1_new,at2_new,am1,am2,nAm1,nAm2,p_iso,p_an,theta1new,theta2,N,theta_int = generate_comm(theta1,theta_interlayer,p_iso_in,p_an_in)
    theta1_calculated.append(theta1new)
    p_an_calc.append(p_an)
    p_iso_calc.append(p_iso)
#    theta2_applied.append(theta2)
    Nm.append(N)
    print(theta2)
    theta_int_calc.append(theta_int)

    amref = np.array([12.897 , 13.217 , 13.011])
    log, am1, am2, am3, alpha1, alpha2 = calculate_woods('HOPG', i, j, k, l, m, n, q, r, 'dir')
    am = np.array([am1, am2, am3])
    lambda_rescaling = np.sum(amref/am)/3
    mean_am = np.sum(am)/3
    #"theoretical" LM
    lm = a*(1+epshbn)/(np.sqrt(epshbn**2+2*(1+epshbn)*(1-np.cos(thetahbn*np.pi/180))))
    lam.append(mean_am)
    phiam.append((alpha1+alpha2)/2)

    # generate moiré commensurate cell
    header = 'Atom_pos_theta_interlayer=%.2f deg, theta_1=%.1f deg, theta_2=%.1f deg, p_iso=%.4f, p_an=%.4f'%(theta_interlayer,theta1new,theta2,p_iso_in,p_an_in)
    print('# '+str(header)+'\n')
    gx,gy,sx,sy,log3 = generateAtomsPos(i,j, k, l, m, n, q, r, N, header)
    # gx,gy,sx,sy,log3 = comm.determine_atoms_fast(i,j, k, l, m, n, q, r, 1, header)

    if log:
        with open('Gen_files/last_log.txt', "w") as f:
            f.write(log1+log2+log3)

figure = 1
if figure:
    fig = plt.figure()
    ax = fig.subplots()
    ax1 = ax.twinx()
    ax1.tick_params(axis='y', colors='r')
    ax1.spines["right"].set_position(("axes", 1.2))
    ax1.spines["right"].set_visible(True)
    ax1.yaxis.label.set_color('r')
    ax2 = ax.twinx()
    ax3 = ax.twinx()
    ax3.yaxis.label.set_color('green')
    ax3.tick_params(axis='y', colors='green')
    ax3.spines["right"].set_position(("axes", 1.4 ))
    ax3.spines["right"].set_visible(True)
    plotam = 0
    if plotam:
        ax.plot(theta1_applied, lam, 'x')
        ax.set_ylabel('$|L_{m}|$ (nm)')
        ax2.plot(theta1_applied, p_an_calc, '1', label='$p_{an}$')
        ax2.plot(theta1_applied, p_iso_calc, '2', label='$p_{iso}$')
        ax1.plot(theta1_applied, theta_int_calc, 'o', color = 'r')
        ax3.plot(theta1_applied, phiam, 'd', color = 'g')
        ax2.set_ylabel('$p_{iso}, p_{an}$')
        ax1.set_ylabel('$\\theta_{int}$ (°)')
        ax3.set_ylabel('$\\Phi_{am}$ (°)')
        ax.set_xlabel('$\\epsilon_{bi}^{applied}')

    else:
        ax4 = ax.twinx()
        # ax3.set_ylim(-0.2, 10.5)
        coef = np.polyfit(theta1_applied, theta1_calculated, 1)
        poly1d_fn = np.poly1d(coef)
        ax.plot(theta1_applied, theta1_calculated, 'x')
        ax1.plot(theta1_applied, theta_int_calc, 'xr')
        ax1.set_ylabel('$\\theta_{int}$ (°)')
        # ax1.set_ylim(1.05, 1.15)
        ax3.plot(theta1_applied, Nm, '--g')
        ax3.set_ylabel('Cell order')
        ax2.plot(theta1_applied, p_an_calc, '1', label='$p_{an}$')
        ax2.plot(theta1_applied, p_iso_calc, '2', label='$p_{iso}$')
        ax.plot(theta1_applied, poly1d_fn(theta1_applied), '--k', label=coef)
        ax.set_xlabel('$\\theta_1^{applied}$ (°)')
        ax.set_ylabel('$\\theta_1^{calculated}$ (°)')
        ax.legend()
        ax2.legend()
        ax4.tick_params(axis='y', colors='gold')
        ax4.spines["right"].set_position(("axes", 1.4 ))
        ax4.spines["right"].set_visible(True)
        ax4.plot(theta1_applied, lam, 'p', color = 'gold', label = '$\\lambda$')
#    plt.tight_layout()
    fig.subplots_adjust(right=0.6)
    plt.show()
