#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 21 10:40:56 2021

@author: florie

Calculates atomic positions for a uniaxial strain cell **in armchair situation only**.
TODO : make more versatile

To get coordinates of an atom from the upper (sheared) layer, click on a red dot.

"""
from __future__ import print_function
import pylab
import numpy as np
import matplotlib.pyplot as plt
from Common import constants as const
from common_AtomPos import getatompos
from generate import generate_comm_unimoire, uniaxial_generateAtomsPos

figure = pylab.figure()
plt.style.use('/home/florie/Documents/LABO/Python/nature.mplstyle') #pour avoir les jolies ecritures

cid = figure.canvas.mpl_connect('button_press_event', lambda event: getatompos(event, gx, gy, header))    

# Save log ?
log = True
log1 = ""
log2 = ""
log3 = ""

pan_applied = []
p_an_calc = []
p_iso_calc = []
Nm = []
lam = []
am1_list = []
am2_list = []
at1_list = []
ag1_list = []
thetas_applied = []
i_index = []
m_index = []

for index in [0]:# range(0, 120, 1):#[30*8]:  # range(20*8, 40*8, 1):
    ii = index/(1.)
    if ii == 90 : continue
    print(ii)
    a = const.lattice_params['gr']
    eps = 1/100
    thetas, theta_interlayer, p_iso_in, p_an_in = ii, 0, 1.0-0.0000, 1+eps
    pan_applied.append(p_an_in)
    thetas_applied.append(ii)
    i,j,k,l,m,n,q,r,ab1,ab2,at1_new,at2_new,am1,am2,nAm1,nAm2,p_iso,p_an,theta1new,theta2,N,theta_int = generate_comm_unimoire(thetas,theta_interlayer,p_iso_in,p_an_in)
    p_an_calc.append(p_an)
    i_index.append(i)
    m_index.append(m)
    Nm.append(N)
    am1_list.append(list(am1))
    am2_list.append(list(am2))
    at1_list.append(list(at1_new))
    ag1_list.append(list(ab1))
    print(p_iso,p_an,theta1new,theta2,N,theta_int)

    print(am1, am2, nAm1, nAm2)

    # generate moiré commensurate cell
    header = 'Atom_pos_uniaxial strain p_an=%.4f'%(p_an_in)
    print('# '+str(header)+'\n')
    generate = 0
    if generate:
        # gx,gy,sx,sy,log3 = comm.generateAtomsPos(i,j, k, l, m, n, q, r, 1, header)
        # gx,gy,sx,sy,log3 = comm.determine_atoms_fast(i,j, k, l, m, n, q, r, 1, header)
        # if am1[1]<10**-4: am1[1] = 0
        j=3
        am2 = np.array([0, 2*j*ab2[1]])
        nAm1 = np.linalg.norm(am1)
        nAm2 = np.linalg.norm(am2)
        print(am1, am2, nAm1, nAm2, i, m)
        print('\n i='+str(i)+' m='+str(m))
        gx,gy,sx,sy,log3 = uniaxial_generateAtomsPos(i,m,j,ab1,ab2,at1_new,at2_new,am1,am2,nAm1,nAm2,N,header,c_gr='red',c_s='blue') 

    if log:
        with open('Gen_files/last_log.txt', "w") as f:
            f.write(log1+log2+log3)

figure = 1
if figure:
    fig = plt.figure()
    ax = fig.subplots()
    ax.set_ylabel('strain (%)')
    ax.set_ylim((np.array(pan_applied)[0]-1)*100-0.01, (np.array(pan_applied)[0]-1)*100+0.01)
    ax.set_ylabel('strain (%)')
    ax.plot(thetas_applied, (np.array(p_an_calc)-1)*100, '1', label='$p_{an}^{calc}$')
    ax.plot(thetas_applied, (np.array(pan_applied)-1)*100, '2', label='$p_{an}^{applied}$')
    ax.legend()

    ax1 = ax.twinx()
    ax1.tick_params(axis='y', colors='g')
    ax1.spines["right"].set_position(("axes", 1.6))
    ax1.spines["right"].set_visible(True)
    ax1.yaxis.label.set_color('g')
    ax1.plot(thetas_applied, Nm, '--g')
    ax1.set_ylabel('Cell order')

    ax2 = ax.twinx()
    ax2.tick_params(axis='y', colors='r')
    ax2.set_ylabel('commensurability indexes')
    # ax2.set_ylim(min(i_index[0],m_index[0])-5, max(i_index[0],m_index[0])+5)
    ax2.plot(thetas_applied, i_index, '+', label='i', color='r')
    ax2.plot(thetas_applied, m_index, 'x', label='m', color='r')
    ax2.spines["right"].set_position(("axes", 1.4))
    ax2.yaxis.label.set_color('r')
    ax2.legend()

    at1 = np.array(at1_list)
    ag1 = np.array(ag1_list)
    ax4 = ax.twinx()
    ax4.tick_params(axis='y', colors='b')
    ax4.spines["right"].set_position(("axes", 1.2))
    ax4.yaxis.label.set_color('b')
    ax4.set_ylabel('graphene periodicty lenghth')
    ax4.plot(thetas_applied, np.linalg.norm(at1, axis=1), '+', label='Nat1', color='b')
    ax4.plot(thetas_applied, np.linalg.norm(ag1, axis=1), 'x', label='Nag1', color='b')
    ax4.legend()
    ax4.set_ylim_min=0

    am1 = np.array(am1_list)
    am2 = np.array(am2_list)
    ax3 = ax.twinx()
    ax3.set_ylabel('Moire lenghth')
    # ax3.plot(thetas_applied, nAm1, 'o', label='nam1')
    # ax3.plot(thetas_applied, am1[:,1], 'o', label='am1[1]')
    ax3.plot(thetas_applied, np.linalg.norm(am1, axis=1), 's', label='Nam1')
    ax3.plot(thetas_applied, np.linalg.norm(am2, axis=1), 'o', label='Nam1')
    ax3.legend()
    ax3.set_ylim(0, 100000000)

#    plt.tight_layout()
    fig.subplots_adjust(right=0.6)
    plt.show()
