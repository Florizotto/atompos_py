#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 14 14:11:49 2021

@author: florie

Calculates atomic positions for a uniaxial shear cell in zigzag or armchair situations.

L(ZZ) = 3*a_c*n
L(AC) = np.sqrt(3)*a_c*n

To get coordinates of an atom from the upper (sheared) layer, click on a red dot.
"""

from __future__ import print_function
import pylab
import numpy as np
import matplotlib.pyplot as plt
import matplotlib 
from common_AtomPos import getatompos
from generate import generateAtomsPosShear
import constants as const

figure = pylab.figure()
plt.style.use('/home/florie/Documents/LABO/Python/nature.mplstyle') #pour avoir les jolies ecritures


cid = figure.canvas.mpl_connect('button_press_event', lambda event: getatompos(event, gx, gy, header))    


def Dirac_displacement(Sh, ab1, ab2): 
    '''
    Predict displacement of sheared **monolayer** for low energy approximation.
    A is the displacement of the cone.
    Explicit derivation for each Dirac Cone.
    '''
    a0 = 1 #a graphene, coherence avec au dessus
    
    # Define basisvectors of bottom graphene in orthogonal basis
    unit = 2*np.pi/(a0)
    kb3 = np.array([-unit, (1/np.sqrt(3))*unit])
    kb1 = np.array([unit, (1/np.sqrt(3))*unit])
    kb2 = np.array([0, (2/np.sqrt(3))*unit])

    kt1 =  np.dot(np.linalg.inv(np.transpose(Sh)), kb1)
    kt2 = np.dot(np.linalg.inv(np.transpose(Sh)), kb2)
    kt3 = np.dot(np.linalg.inv(np.transpose(Sh)), kb3)

    K1 = (kb1-2*kb2)/3
    K2 = (2*kb1- kb2)/3
    K3 = (kb2 + kb1)/3
    Kd1 = (kt1-2*kt2)/3
    Kd2 = (2*kt1- kt2)/3
    Kd3 = (kt2 + kt1)/3
    # print(np.linalg.norm(Kd1), np.linalg.norm(Kd2), np.linalg.norm(Kd3))

    # Find NN and deformed NN
    delta3 = -(2/3)*ab1-(1/3)*ab2
    delta2 = (1/3)*ab1-(1/3)*ab2
    delta1 = (1/3)*ab1+(2/3)*ab2
    deltat3 = -(2/3)*np.dot(Sh, ab1)-(1/3)*np.dot(Sh, ab2)
    deltat2 = (1/3)*np.dot(Sh, ab1)-(1/3)*np.dot(Sh, ab2)
    deltat1 = (1/3)*np.dot(Sh, ab1)+(2/3)*np.dot(Sh, ab2)
    
    
    #Find Delta NN
    Ddelta1 = -delta1+deltat1
    Ddelta2 = -delta2+deltat2
    Ddelta3 = -delta3+deltat3

    #Find du
    du1 = np.dot(delta1, (Ddelta1))/a0*np.sqrt(3)
    du2 = np.dot(delta2, (Ddelta2))/a0*np.sqrt(3)
    du3 = np.dot(delta3, (Ddelta3))/a0*np.sqrt(3)
    
    param = 0.5*const.dtdr/(const.vF*const.hbar) # eV / nm ; plotté dans la ZDB ! Avec definition / 2 pour cohérence entre 2 definitions de la mZB
    
    #Attention j'ai mis Kdi ou Ki ?
    GF1 = param*np.exp(1j*4*np.pi/3)*(du1*np.exp(-1j*np.dot(delta1, Kd1))+
                                      du2*np.exp(-1j*np.dot(delta2, Kd1))+du3*np.exp(-1j*np.dot(delta3, Kd1)))
    D1 = [-GF1.real, GF1.imag]
    GF2 = param*(du1*np.exp(-1j*np.dot(delta1, Kd2))+
                 du2*np.exp(-1j*np.dot(delta2, Kd2))+du3*np.exp(-1j*np.dot(delta3, Kd2)))
    D2 = [GF2.real, GF2.imag]
    GF3 = param*np.exp(-1j*4*np.pi/3)*(du1*np.exp(-1j*np.dot(delta1, Kd3))
                                       +du2*np.exp(-1j*np.dot(delta2, Kd3))+du3*np.exp(-1j*np.dot(delta3, Kd3)))
    D3 = [-GF3.real, GF3.imag]
    GF4 = param*np.exp(-1j*4*np.pi/3)*(du1*np.exp(-1j*np.dot(delta1, -Kd1))+
                                       du2*np.exp(-1j*np.dot(delta2, -Kd1))+du3*np.exp(-1j*np.dot(delta3, -Kd1)))
    D4 = [GF4.real, GF4.imag]
    GF5 = param*(du1*np.exp(-1j*np.dot(delta1, -Kd2))+
                 du2*np.exp(-1j*np.dot(delta2, -Kd2))+du3*np.exp(-1j*np.dot(delta3, -Kd2)))
    D5 = [-GF5.real, GF5.imag]
    GF6 = param*np.exp(1j*4*np.pi/3)*(du1*np.exp(-1j*np.dot(delta1, -Kd3))+
                                      du2*np.exp(-1j*np.dot(delta2, -Kd3))+du3*np.exp(-1j*np.dot(delta3, -Kd3)))
    D6 = [GF6.real, GF6.imag]
    
    
    return(Kd1, Kd2, Kd3, D1, D2, D3, D4, D5, D6)


def Armchair_cell(n):
    #parameters for shear zigzag
    alpha = np.arctan(3/(np.sqrt(3)*n))
    print(alpha*180/np.pi)
    eps = np.tan(alpha)
    #shear matrix
    Sh = np.array([[1,0], [eps, 1]])
    
    Nmaille = 1
    
    #Define basisvectors of substrate in units of a_TM
    ab1=np.array([1.,0])
    ab2=np.array([-1./2,np.sqrt(3)/2])
    
    #Define moiré vector
    am1=n*ab1
    m=1
    am2 = np.array([0, 2*m*ab2[1]])
    
    #Find graphene vector
    at1=np.dot(Sh, ab1)
    at2=np.dot(Sh, ab2)
    return alpha,Sh,ab1,ab2,at1,at2,am1,am2,n,m,Nmaille

def ZigZag_cell(n):
    alpha = np.arctan(1/(np.sqrt(3)*n))
    print(alpha*180/np.pi)
    eps = np.tan(alpha)
    #shear matrix
    Sh = np.array([[1,eps], [0, 1]])
    
    Nmaille = 1
    
    #Define basisvectors of substrate in units of a_TM
    ab1=np.array([1.,0])
    ab2=np.array([-1./2,np.sqrt(3)/2])
    
    #Define moiré vector
    m = 1
    am1 = m*ab1
    am2 = n*np.array([0,np.sqrt(3)])
    
    #Find graphene vector
    at1=np.dot(Sh, ab1)
    at2=np.dot(Sh, ab2)
    ab1 = -ab1-ab2
    at1 = -at1-at2
    
    return alpha,Sh,ab1,ab2,at1,at2,am1,am2,n,m,Nmaille
    
#%% main single cell generation
generatecell = 1
Armchair = 1

if Armchair:
    
    nn = 1
    nn = 3*nn
    alpha,Sh,ab1,ab2,at1,at2,am1,am2,n,m,Nmaille = Armchair_cell(nn)
    
    header = 'Atom_pos_alpha_shear=%.1f deg, n=%.1f, m=%.1f'%(alpha*180/np.pi,n,2*m)
    print(header)
    if generatecell:
        gx, gy, gxs, gys, log = generateAtomsPosShear(ab1,ab2,at1,at2,am1,am2,n,2*m,header, Nmaille)

else:  # ZigZag
    #parameters for shear zigzag
    nn = 550
    alpha,Sh,ab1,ab2,at1,at2,am1,am2,n,m,Nmaille = ZigZag_cell(nn)
    
    header = 'Atom_pos_alpha_shear_ZZ=%.1f deg, n=%.1f, m=%.1f'%(alpha*180/np.pi,n,m)
    print(header)
    if generatecell:
        gx, gy, gxs, gys, log = generateAtomsPosShear(ab1,ab2,at1,at2,am1,am2,n,m,header, Nmaille)

#%% Dirac cones displacement of single sheared layer
cmap = matplotlib.cm.get_cmap('afmhot')

displacement = 0
if displacement:
    Kd1list=[]
    Kd2list=[]
    Kd3list=[]
    A1list=[]
    A2list=[]
    A3list=[]
    A4list=[]
    A5list=[]
    A6list=[]
    
    nmax=200
    for nn in range(2, nmax, 1):
        
        alpha,Sh,ab1,ab2,at1,at2,am1,am2,n,m,Nmaille = Armchair_cell(nn)
        # alpha,Sh,ab1,ab2,at1,at2,am1,am2,n,m,Nmaille = ZigZag_cell(nn)

        dk1, dk2, dk3, A1, A2, A3, A4, A5, A6 = Dirac_displacement(Sh, ab1, ab2)
        #!!!
        Kd1list.append(dk1)
        Kd2list.append(dk2)
        Kd3list.append(dk3)
        A1list.append(A1)
        A2list.append(A2)
        A3list.append(A3)
        A4list.append(A4)
        A5list.append(A5)
        A6list.append(A6)

        figureBZtest = 1
        if nn == 2 and figureBZtest:
            figF = plt.figure()
            axF = figF.subplots()
            axF.set_xlabel('$k_x(nm^{-1}$')
            axF.set_ylabel('$k_y(nm^{-1}$')
            axF.set_ylim(-10, 10)
            axF.set_xlim(-10, 10)
            
            #plot BZ
            plt.plot([-dk3[0],dk1[0]],[-dk3[1],dk1[1]],color='k')
            pylab.plot([dk3[0],-dk1[0]],[dk3[1],-dk1[1]],color='k')
            pylab.plot([dk1[0],dk2[0]],[dk1[1],dk2[1]],color='k')
            pylab.plot([-dk1[0],-dk2[0]],[-dk1[1],-dk2[1]],color='k')
            pylab.plot([dk2[0],dk3[0]],[dk2[1],dk3[1]],color='k')
            pylab.plot([-dk2[0],-dk3[0]],[-dk2[1],-dk3[1]],color='k')

            #Plot A(K)
            pylab.plot([dk1[0],dk1[0]+A3[0]],[dk1[1],dk1[1]+A3[1]],color='purple')
            pylab.plot([-dk3[0],-dk3[0]+A2[0]],[-dk3[1],-dk3[1]+A2[1]],color='lightblue')
            pylab.plot([-dk2[0],-dk2[0]+A1[0]],[-dk2[1],-dk2[1]+A1[1]],color='purple')
            pylab.plot([dk2[0],dk2[0]+A4[0]],[dk2[1],dk2[1]+A4[1]],color='lightblue')
            pylab.plot([dk3[0],dk3[0]+A5[0]],[dk3[1],dk3[1]+A5[1]],color='purple')
            pylab.plot([-dk1[0],-dk1[0]+A6[0]],[-dk1[1],-dk1[1]+A6[1]],color='lightblue')
            
        if nn == nmax-1:
            figBZ, axBZ = plt.subplots()
            axBZ.set_xlabel('$k_x(nm^{-1}$')
            axBZ.set_ylabel('$k_y(nm^{-1}$')
            
            #plot undeformed BZ
            a0 = 1
            unit = 2*np.pi/(a0)
            kb3 = np.array([-unit, (1/np.sqrt(3))*unit])
            kb1 = np.array([unit, (1/np.sqrt(3))*unit])
            kb2 = np.array([0, (2/np.sqrt(3))*unit])
        
            K1 = (kb1-2*kb2)/3
            K2 = (2*kb1- kb2)/3
            K3 = (kb2 + kb1)/3
            plt.plot([-K3[0],K1[0]],[-K3[1],K1[1]],color='k')
            pylab.plot([K3[0],-K1[0]],[K3[1],-K1[1]],color='k')
            pylab.plot([K1[0],K2[0]],[K1[1],K2[1]],color='k')
            pylab.plot([-K1[0],-K2[0]],[-K1[1],-K2[1]],color='k')
            pylab.plot([K2[0],K3[0]],[K2[1],K3[1]],color='k')
            pylab.plot([-K2[0],-K3[0]],[-K2[1],-K3[1]],color='k')
            
            Kd1list = np.array(Kd1list)
            Kd2list = np.array(Kd2list)
            Kd3list = np.array(Kd3list)
            A1list = np.array(A1list)
            A2list = np.array(A2list)
            A3list = np.array(A3list)
            A4list = np.array(A4list)
            A5list = np.array(A5list)
            A6list = np.array(A6list)
            # axBZ.scatter(Kd1list[:,0],Kd1list[:,1],color='purple')
            # axBZ.scatter(-Kd2list[:,0],-Kd2list[:,1],color='purple')
            # axBZ.scatter(Kd3list[:,0],Kd3list[:,1],color='purple')
            # axBZ.scatter(-Kd1list[:,0],Kd1list[:,1],color='lightblue')
            # axBZ.scatter(Kd2list[:,0],-Kd2list[:,1],color='lightblue')
            # axBZ.scatter(-Kd3list[:,0],Kd3list[:,1],color='lightblue')
            
            axBZ.scatter(Kd1list[:,0]+A3list[:,0],Kd1list[:,1]+A3list[:,1],color='purple')
            axBZ.scatter(-Kd2list[:,0]+A1list[:,0],-Kd2list[:,1]+A1list[:,1],color='purple')
            axBZ.scatter(Kd3list[:,0]+A5list[:,0],Kd3list[:,1]+A5list[:,1],color='purple')
            axBZ.scatter(-Kd1list[:,0]+A6list[:,0],-Kd1list[:,1]+A6list[:,1],color='lightblue')
            axBZ.scatter(Kd2list[:,0]+A4list[:,0],Kd2list[:,1]+A4list[:,1],color='lightblue')
            axBZ.scatter(-Kd3list[:,0]+A2list[:,0],-Kd3list[:,1]+A2list[:,1],color='lightblue')

            